﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Globalization;


namespace CSharpHelpers
{
  public enum ExifOrientations
  {
    None = 0,
    Normal = 1,             // ORIENTATION_TOPLEFT
    HorizontalFlip = 2,     // ORIENTATION_TOPRIGHT
    Rotate180 = 3,          // ORIENTATION_BOTRIGHT
    VerticalFlip = 4,       // ORIENTATION_BOTLEFT
    Transpose = 5,          // ORIENTATION_LEFTTOP
    Rotate270 = 6,          // ORIENTATION_RIGHTTOP
    Transverse = 7,         // ORIENTATION_RIGHTBOT
    Rotate90 = 8            // ORIENTATION_LEFTBOT
  }

  public enum ImageStates
  {
    Unloaded,
    Loading,
    Loaded
  }

  public class DisplayableImage : INotifyPropertyChanged
  {
    public const int TargetScreenResolutionMaxDim = 512;
    protected const int TargetMediumResolutionHeight = 352;
    protected const int TargetThumbnailResolutionHeight = 120;

    public String AssetURL { get; set; }

    private static int _defaultThumbnailSize = TargetThumbnailResolutionHeight;
    public static int DefaultThumbnailSize
    {
      get { return _defaultThumbnailSize; }
      set
      {
        if (value != _defaultThumbnailSize)
          _defaultThumbnailSize = value;
      }
    }

    public DisplayableImage(string path)
    {
      AssetURL = path;
    }

    private int _height = 0;
    public int Height
    {
      get
      {
        if ((_height == 0) && !string.IsNullOrWhiteSpace(AssetURL))
        {
          BitmapFrame frame = BitmapFrame.Create(new Uri(AssetURL), BitmapCreateOptions.None, BitmapCacheOption.None);
          _height = frame.PixelHeight;
          _width = frame.PixelWidth;
          NotifyPropertyChanged("Height");
          NotifyPropertyChanged("ScreenResHeight");
          NotifyPropertyChanged("MediumResHeight");
        }
        return _height;
      }
      protected set
      {
        if (value != _height)
        {
          _height = value;
          NotifyPropertyChanged("Height");
          NotifyPropertyChanged("ScreenResHeight");
          NotifyPropertyChanged("MediumResHeight");
        }
      }
    }

    private int _width = 0;
    public int Width
    {
      get
      {
        if ((_width == 0) && !string.IsNullOrWhiteSpace(AssetURL))
        {
          BitmapFrame frame = BitmapFrame.Create(new Uri(AssetURL), BitmapCreateOptions.None, BitmapCacheOption.None);
          _height = frame.PixelHeight;
          _width = frame.PixelWidth;
          NotifyPropertyChanged("Width");
          NotifyPropertyChanged("ScreenResWidth");
          NotifyPropertyChanged("MediumResWidth");
        }
        return _width;
      }
      protected set
      {
        if (value != _width)
        {
          _height = value;
          NotifyPropertyChanged("Width");
          NotifyPropertyChanged("ScreenResWidth");
          NotifyPropertyChanged("MediumResWidth");
        }
      }
    }



    public virtual ExifOrientations Orientation { get; protected set; }

    public virtual String ToolTip { get { return AssetURL; } }

    private static BitmapSource GetTextualBitmap(string msg, double height, double width)
    {
      RenderTargetBitmap bmp = null;
      try
      {
        DrawingVisual drawing = new DrawingVisual();
        DrawingContext dc = drawing.RenderOpen();

        FormattedText label = new FormattedText(String.Format(msg),
                                CultureInfo.GetCultureInfo("en-us"),
                                FlowDirection.LeftToRight,
                                new Typeface("Verdana"),
                                14, Brushes.Red, 1.0);
        label.SetFontWeight(FontWeights.Bold, 0, msg.Length);
        label.SetFontSize(18, 0, msg.Length);
        label.SetForegroundBrush(Brushes.Black, 0, msg.Length);
        label.TextAlignment = TextAlignment.Center;
        dc.DrawText(label, new Point(width / 2, height / 2));

        dc.Close();
        bmp = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
        bmp.Render(drawing);
      }
      catch
      {
      }
      return bmp;
    }

    private static BitmapSource _brokenBitmap = null;
    private static BitmapSource BrokenBitmap
    {
      get
      {
        if (_brokenBitmap == null)
        {
          _brokenBitmap = GetTextualBitmap("Broken", 200, 200);
        }
        return _brokenBitmap;
      }
    }

    private static BitmapSource _loadingBitmap = null;
    private static BitmapSource LoadingBitmap
    {
      get
      {
        if (_loadingBitmap == null)
        {
          _loadingBitmap = GetTextualBitmap("Loading", 200, 200);
        }
        return _loadingBitmap;
      }
    }

    /// <summary>
    /// Obtains image thumbnail (including rotation)
    /// (heavily) modified from http://slouge.wordpress.com/2009/10/28/wpf-fast-thumbnails-image-generation-rotation/
    /// Obtient le thumnail de l'image source (incluant la rotation)
    /// obtains the thumbnail from an image, applying rotation
    /// This code, per Dan Gross, is okay for internal use but is NOT cleared for commercialization
    /// </summary>
    /// <returns>BitmapSource of image, or null if image handler was specified and image is not loaded</returns>
    /// 
    public BitmapSource GetImage(int size)
    {
      BitmapSource ret = null;
      BitmapMetadata meta = null;
      double angle = 0;
      String url = AssetURL;

      // N.B. if remote, must use BitmapCreateOptions.None when creating BitmapImage
      // Otherwise nothing is displayed
	  bool isRemote = url.StartsWith("http");
      BitmapCreateOptions bco = isRemote ? BitmapCreateOptions.None : BitmapCreateOptions.DelayCreation;

      //tentative de creation du thumbnail via Bitmap frame : très rapide et très peu couteux en mémoire !
      // initial creation of the thumbnail using a Bitmap frame : very rapid and low cost in memory
	  BitmapFrame frame = null;
	  try
	  {
		// have observed timeout errors here, hence the try/catch
		frame = BitmapFrame.Create(new Uri(url), bco, BitmapCacheOption.None);
	  }
	  catch (Exception e)
	  {
		System.Int32 HR = e.HResult;
		LoggingHelper.Instance.LogError(this,string.Format("{0}#{1}: exception; file={2}, S/F/C={3}/{4}/{5}, msg={6}{7}{8}",
			LoggingHelper.ThisFunctionName(),LoggingHelper.ThisLineNumber(),url.ToString(),(HR>>32)&1,(HR>>16)&0x7ff,HR&0xffff,e.Message,
			Environment.NewLine,e.StackTrace));
		System.Diagnostics.Debug.WriteLine("GetImage() exception on {0}\n{1}", url.ToString(), e.Message);
		ret = BrokenBitmap;
		return ret;
	  }

      try
      {
        meta = frame.Metadata as BitmapMetadata;

        Height = frame.PixelHeight;
        Width = frame.PixelWidth;

        //var exifHeight = meta.GetQuery("/app1/ifd/exif/subifd:{uint=40962}");
        //var exifWidth = meta.GetQuery("/app1/ifd/exif/subifd:{uint=40963}");
        //Height = Convert.ToInt32(exifHeight);
        //Width = Convert.ToInt32(exifWidth);

        if (meta != null)//si on a des meta, tentative de récupération de l'orientation
        {
          Orientation = ExifOrientations.Normal;
          //if (meta.GetQuery("/app1/ifd/{ushort=274}") != null)
          //  Orientation = (ExifOrientations)Enum.Parse(typeof(ExifOrientations), meta.GetQuery("/app1/ifd/{ushort=274}").ToString());
          string orientation_field = meta.GetQuery("/app1/ifd/{ushort=274}")?.ToString();
          if (!string.IsNullOrWhiteSpace(orientation_field))
            Orientation = (ExifOrientations)Enum.Parse(typeof(ExifOrientations), orientation_field);

          switch (Orientation)
          {
            case ExifOrientations.Rotate90:
              angle = -90;
              break;
            case ExifOrientations.Rotate180:
              angle = 180;
              break;
            case ExifOrientations.Rotate270:
              angle = 90;
              break;
          }
        }

        // Note that we are only considering the thumbnail height in determining whether we should use the
        // Exif thumbnail or not.  Depending on the image orientation, this may not really be the right test.
        if ((frame.Thumbnail == null) ||                                                                            // no thumbnail
            (size > Math.Max(frame.Thumbnail.PixelHeight, frame.Thumbnail.PixelWidth)) ||                           // thumbnail too small
            ((frame.Thumbnail.PixelWidth > frame.Thumbnail.PixelHeight) != (frame.PixelWidth > frame.PixelHeight))) // thumbnail orientation questionable
        //if ((frame.Thumbnail == null) ||                                                                            // no thumbnail
        //    (size > Math.Max(frame.Thumbnail.PixelHeight, frame.Thumbnail.PixelWidth)))                             // thumbnail too small
        {
          if (size == 0 || size >= TargetScreenResolutionMaxDim || size >= Height)
          {
            // Minimize quality loss issues
            ret = frame;
          }
          else
          {
            // N.B. the following does result in some image degradation
            // The following method is based on the approach used in "PhotoLoader"
            // http://dotnetlearning.wordpress.com/2011/01/27/loading-images-asynchronously-in-wpf/
            // Specifically, the code below follows the paradigm in Manager.cs:GetBitmapSource
            // A naive approach of just setting Source to the url will lock up and consume large amounts of memory
            // if applied to a large number of high resolution images.  This is true even if BitmapCacheOption is set
            // to OnLoad.  The approach appears to completely disconnect the source from the final result.

            TransformedBitmap image = new TransformedBitmap();

            image.BeginInit();
            image.Source = frame as BitmapSource;
            int origHeight = frame.PixelHeight;
            int origWidth = frame.PixelWidth;

            int decodeHeight = Math.Min(size, origHeight);
            int decodeWidth = (origWidth * decodeHeight) / origHeight;

            double xScale = (double)decodeWidth / (double)origWidth;
            double yScale = (double)decodeHeight / (double)origHeight;

            TransformGroup transGroup = new TransformGroup();
            transGroup.Children.Add(new ScaleTransform(xScale, yScale));
            image.Transform = transGroup;
            if (size >= TargetMediumResolutionHeight)
              RenderOptions.SetBitmapScalingMode(image, BitmapScalingMode.HighQuality);
            image.EndInit();

            WriteableBitmap image2 = new WriteableBitmap(image);
            image2.Freeze();
            ret = image2;
          }
        }
        else
        {
          //récupération des métas de l'image
          ret = frame.Thumbnail;
          //if ((Math.Abs(angle) == 90) &&
          //    ((frame.Thumbnail.PixelWidth > frame.Thumbnail.PixelHeight) != (frame.PixelWidth > frame.PixelHeight)))
          //  angle = 180;
        }

        if (ret != null && angle != 0) //on doit effectuer une rotation de l'image
        {
          ret = new TransformedBitmap(ret.Clone(), new RotateTransform(angle));
          ret.Freeze();
        }
      }
      catch (Exception e)
      {
		// exceptions were occurring, hence the detailed diagnostic output
		System.Int32 HR = e.HResult;
        LoggingHelper.Instance.LogError(this,string.Format("{0}#{1}: exception; file={2}, S/F/C={3}/{4}/{5}, msg={6}{7}{8}",
			LoggingHelper.ThisFunctionName(),LoggingHelper.ThisLineNumber(),url.ToString(),(HR>>32)&1,(HR>>16)&0x7ff,HR&0xffff,e.Message,
			Environment.NewLine,e.StackTrace));
        System.Diagnostics.Debug.WriteLine("GetImage() exception on {0}\n{1}", url.ToString(), e.Message);
        ret = BrokenBitmap;
      }

      return ret;
    }

    #region Thumbnail ---------------------------------------------------------------------------------------

	// SEE COMMENTS IN FUNCTION WaitForThumbnailTaskCount() BELOW
	private static int _thumbnailTaskCountLimit = 8;	// no more than this many active tasks
	private static int _thumbnailTaskSleepMSec = 5;		// delay time while waiting for _thumbnailTaskCount < _thumbnailTaskCountLimit
	private static bool _thumbnailTaskFirst = true;
	private static int _thumbnailTaskCount = 0;
	private static Object _thumbnailTaskCountLock = new Object();
	public int ThumbnailTaskCount
	{
		get
		{
			lock(_thumbnailTaskCountLock)
			{
				return _thumbnailTaskCount;
			}
		}
	}
	private int IncThumbnailTaskCount  ()
	{
		lock(_thumbnailTaskCountLock)
		{
			return ++_thumbnailTaskCount;
		}
	}
	private int DecThumbnailTaskCount  ()
	{
		lock(_thumbnailTaskCountLock)
		{
			_thumbnailTaskFirst = false;
			return --_thumbnailTaskCount;
		}
	}
	private async Task<int> WaitForThumbnailTaskCount  ()
	{
		// The original version of the "SmartThumbnail" code worked fine when the image
		// directory to be loaded was specified using a local hard drive path or a UNC
		// path.  However, when a mapped drive path was specified, undesirable behavior
		// (hang/crash) would occur if the thumbnail generation code was allowed to run
		// an unrestricted number of tasks.  My guess is that this is a Windows problem;
		// i.e., the code that processes mapped drive paths gets overwhelmed by too many
		// requests at once and then crashes and burns.
		//
		// To avoid this, the each asynchronous task calls this function before proceeding
		// with thumbnail generation.  This function waits (giving up the CPU without
		// blocking) until the thumbnail task count drops below an upper limit before
		// incrementing the task count and returning.  I.e., only a limited number of
		// thumbnail generation tasks are allowed to actually execute at any one time.
		//
		// EXCEPT FOR the first time, where an upper limit of 1 is imposed, again because
		// of observed undesirable behavior (significant delay in getting tasks started).
		// My theory about this is that Windows must be given a chance to go through it's
		// first time initializations before addtional task requests are made, otherwise
		// it appears to thrash.
		//int DelayLoopCount = 0;
		while  ( true )
		{
			lock(_thumbnailTaskCountLock)
			{
				int TaskCountLimit = _thumbnailTaskFirst ? 1 : _thumbnailTaskCountLimit;
				_thumbnailTaskFirst = false;
				if ( _thumbnailTaskCount < TaskCountLimit )
				{
					//if  ( DelayLoopCount > 0 )	// DIAGNOSTIC OUTPUT
					//  System.Diagnostics.Debug.WriteLine("{0}#{1}T{2}: delay {3}",LoggingHelper.ThisFunctionName(),
					//	LoggingHelper.ThisLineNumber(),Thread.CurrentThread.ManagedThreadId,DelayLoopCount);
					return ++_thumbnailTaskCount;
				}
			}
			await Task.Delay(_thumbnailTaskSleepMSec);	// give up CPU before checking again
			//DelayLoopCount++;
		}
	}

    private DispatcherOperation _thumbnailDispatcherOp = null;
    private BitmapSource _SmartThumbnail = null;
	private Object _SmartThumbnailLock = new Object();
    public BitmapSource SmartThumbnail
    {
      get
      {
		BitmapSource ret;
		bool DoNotify = false;

		lock(_SmartThumbnailLock)
		{//lock

		  if (_SmartThumbnail == null)
		  {//run task to populate thumbnail

			_SmartThumbnail = LoadingBitmap;	// put something there while the thumbnail is being generated
			DoNotify = true;	// _SmartThumbnail transitioned from null to non-null
			Task.Run(GetSmartThumbnailAsync);
            //_thumbnailDispatcherOp = Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ContextIdle,
            //    new GetThumbnailDelegate(GetSmartThumbnail));

		  }//run task to populate thumbnail

		  ret = _SmartThumbnail;

		}//lock

		if  ( DoNotify )
		  NotifyPropertyChanged("SmartThumbnail");
		return ret;
      }

      protected set
      {
		lock(_SmartThumbnailLock)
		{
          _SmartThumbnail = value;
		}
        NotifyPropertyChanged("SmartThumbnail");
      }
    }

    protected delegate void GetThumbnailDelegate();

    protected void GetSmartThumbnail()
    {
      _thumbnailDispatcherOp = null;
      BitmapSource t = GetImage(DefaultThumbnailSize);
      // if null, still waiting for image to download
      if (null != t)
      {
        SmartThumbnail = t;
      }
    }

    protected async Task GetSmartThumbnailAsync()
    {
	  // the "wait for a task slot" is done here so we don't hang the UI thread
	  await WaitForThumbnailTaskCount();	// limit the number of executing thumbnail generation tasks
      await Task.Run(() =>
      {
        BitmapSource t = GetImage(DefaultThumbnailSize);
        // if null, still waiting for image to download
        if (null != t)
        {
          SmartThumbnail = t;
		  DecThumbnailTaskCount();	// make thumbnail generation task slot available
        }
      });
    }

    public void UnloadThumbnail()
    {
      SmartThumbnail = null;
    }
    #endregion

    #region ScreenRes ---------------------------------------------------------------------------------------
    public int TargetMaxScreenDim
    {
      get
      {
        return Math.Min(Math.Max(Height, Width), TargetScreenResolutionMaxDim);
      }
    }

    public int RenderedHeight
    {
      get
      {
        double h;
        if (ExifOrientations.Rotate90 == Orientation || ExifOrientations.Rotate270 == Orientation)
        {
          h = Width;
        }
        else
        {
          h = Height;
        }

        double maxDim = Math.Max(Height, Width);

        return (int)((maxDim > TargetScreenResolutionMaxDim) ? h * (TargetMaxScreenDim / maxDim) : h);
      }
    }

    public int RenderedWidth
    {
      get
      {
        double w;
        if (ExifOrientations.Rotate90 == Orientation || ExifOrientations.Rotate270 == Orientation)
        {
          w = Height;
        }
        else
        {
          w = Width;
        }

        double maxDim = Math.Max(Height, Width);

        return (int)((maxDim > TargetScreenResolutionMaxDim) ? w * (TargetMaxScreenDim / maxDim) : w);
      }
    }

    private DispatcherOperation _screenResDispatcherOp = null;
    protected ImageStates _screenResState = ImageStates.Unloaded;
    private BitmapSource _ScreenRes = null;
    public virtual BitmapSource ScreenRes
    {
      get
      {
        if (ImageStates.Loaded == _screenResState)
        {
          return _ScreenRes;
        }
        else
        {
          if (ImageStates.Unloaded == _screenResState)
          {
            _screenResState = ImageStates.Loading;
            Task.Run(GetScreenResAsync);
            //_screenResDispatcherOp = Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
            //    new GetThumbnailDelegate(GetScreenRes));
          }
          return LoadingBitmap;
        }
      }

      protected set
      {
        if (value == null)
          _screenResState = ImageStates.Unloaded;
        else
          _screenResState = ImageStates.Loaded;

        _ScreenRes = value;

        NotifyPropertyChanged("ScreenRes");
      }
    }

    public void UnloadScreenRes()
    {
      ScreenRes = null;
    }

    protected void GetScreenRes()
    {
      BitmapSource t = GetImage(TargetScreenResolutionMaxDim);
      if (null != t && _screenResState == ImageStates.Loading)
      {
        ScreenRes = t;
      }
    }

    protected async Task GetScreenResAsync()
    {
      await Task.Run(() =>
      {
        BitmapSource t = GetImage(TargetScreenResolutionMaxDim);
        if (null != t)
        {
          ScreenRes = t;
        }
      });
    }


    #endregion

    #region MediumRes ---------------------------------------------------------------------------------------
    public int MediumResHeight
    {
      get
      {
        double ar = Width / Height;
        //int h = (ar>1) 
        return Math.Min(TargetMediumResolutionHeight, Height);
      }
    }

    private ImageStates _mediumState = ImageStates.Unloaded;
    private BitmapSource _MediumRes = null;
    public BitmapSource MediumRes
    {
      get
      {
        if (ImageStates.Loaded == _mediumState)
        {
          return _MediumRes;
        }
        else
        {
          if (ImageStates.Unloaded == _mediumState)
          {
            _mediumState = ImageStates.Loading;
            Task.Run(GetMediumResAsync);
            //Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle,
            //    new GetThumbnailDelegate(GetMediumRes));
          }
          return LoadingBitmap;
        }
      }

      protected set
      {
        if (value == null)
          _mediumState = ImageStates.Unloaded;
        else
          _mediumState = ImageStates.Loaded;

        _MediumRes = value;

        NotifyPropertyChanged("MediumRes");
      }
    }

    protected void GetMediumRes()
    {
      BitmapSource t = GetImage(MediumResHeight);
      // Only set if we got back an image and we are still in Loading state
      // (Don't want to load if an unload transpired between when the thread
      // to load the medium resolution was dispatched and now)
      if (null != t && _mediumState == ImageStates.Loading)
      {
        MediumRes = t;
      }
    }

    protected async Task GetMediumResAsync()
    {
      await Task.Run(() =>
      {
        BitmapSource t = GetImage(MediumResHeight);
        if (null != t)
        {
          ScreenRes = t;
        }
      });
    }

    public void UnloadMediumRes()
    {
      MediumRes = null;
    }
    #endregion

    public void Unload(bool all = false)
    {
      if (all)
        UnloadThumbnail();
      UnloadMediumRes();
      UnloadScreenRes();
    }

    public event PropertyChangedEventHandler PropertyChanged;
    public void NotifyPropertyChanged(String propertyName)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }

    public void TerminateLoading()
    {
      if (null != _thumbnailDispatcherOp)
        _thumbnailDispatcherOp.Abort();

      if (null != _screenResDispatcherOp)
        _screenResDispatcherOp.Abort();
    }

  }
}

