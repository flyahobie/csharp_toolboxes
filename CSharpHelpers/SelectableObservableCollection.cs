﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Threading;

namespace CSharpHelpers
{
  //=========================================================================================================
  public class SelectableObservableCollection<T> : INotifyPropertyChanged
  {
    // DEBUG
    //-------------------------------------------------------------------------------------------------------
    public string DebugObjectID { get; set; }

    //-------------------------------------------------------------------------------------------------------
    protected ObservableCollection<T> _items = new ObservableCollection<T>();
    protected int _selectedIndex = -1;

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// The collection
    /// </summary>
    public ObservableCollection<T> Items
    {
      get { return _items; }
      set
      {
        int oldCount = (_items != null) ? _items.Count : 0;
        int newCount = (value != null) ? value.Count : 0;
        if (newCount != oldCount)
          SelectedIndex = -1;

        if (_items != value)
        {
          if (_items != null)
            _items.CollectionChanged -= new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_items_CollectionChanged);
          _items = value;
          if (_items != null)
          {
            _items.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_items_CollectionChanged);
          }
          NotifyPropertyChanged("Items");
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// A side effect to changing SelectedIndex is that the SelectedItem is set.
    /// This will cause the selected images to show up in the preview window.
    /// </summary>
    public int SelectedIndex
    {
      get { return _selectedIndex; }
      set
      {
        if (_selectedIndex != value)
        {
          _selectedIndex = value;
          NotifyPropertyChanged("SelectedIndex");
          NotifyPropertyChanged("SelectedItem");
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// </summary>
    public T SelectedItem
    {
      get { return ((_selectedIndex < 0) || (_items.Count == 0)) ? default(T) : _items[_selectedIndex]; }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void _items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      SelectedIndex = -1;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    public void IncrementSelectedIndex()
    {
      if (Items.Count > 0)
        SelectedIndex = (SelectedIndex + 1) % Items.Count;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    public void DecrementSelectedIndex()
    {
      SelectedIndex = (SelectedIndex <= 0) ? Items.Count - 1 : SelectedIndex - 1;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    public void ClearSelection()
    {
      SelectedIndex = -1;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    protected delegate void DelayClearSelectionDelegate();
    public void DelayClearSelection()
    {
      // DispatcherPriority.Invalid         -1
      // DispatcherPriority.Inactive        0
      // DispatcherPriority.SystemIdle      1
      // DispatcherPriority.ApplicationIdle 2
      // DispatcherPriority.ContextIdle     3
      // DispatcherPriority.Background      4
      // DispatcherPriority.Input           5
      // DispatcherPriority.Loaded          6
      // DispatcherPriority.Render          7
      // DispatcherPriority.DataBind        8
      // DispatcherPriority.Normal          9
      // DispatcherPriority.Send            10

      //Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new DelayClearSelectionDelegate(ClearSelection));
      Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() => ClearSelection()));
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    public void EnsureSelection()
    {
      if ((SelectedIndex < 0) && (Items.Count > 0))
        SelectedIndex = 0;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// INotofyPropertyChanged event
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

    //-------------------------------------------------------------------------------------------------------
    public void NotifyPropertyChanged(String propertyName)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }
  }

}
