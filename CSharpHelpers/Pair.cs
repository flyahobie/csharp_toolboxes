﻿
namespace CSharpHelpers
{
  public class pair<T, U>
  {
    public T first { get; set; }
    public U second { get; set; }

    public pair()
    {
    }

    public pair(T First, U Second)
    {
      this.first = First;
      this.second = Second;
    }

    public pair(pair<T, U> a)
    {
      this.first = a.first;
      this.second = a.second;
    }

    public override bool Equals(object obj)
    {
      if (obj == null) return false;
      pair<T, U> a = obj as pair<T, U>;
      if (a == null) return false;
      return first.Equals(a.first) && second.Equals(a.second);
    }

    public override int GetHashCode()
    {
      return first.GetHashCode() + second.GetHashCode();
    }

    public static bool operator ==(pair<T, U> a, pair<T, U> b)
    {
      if (System.Object.ReferenceEquals(a, b)) return true;
      if (((object)a == null) || ((object)b == null)) return false;
      return a.first.Equals(b.first) && a.second.Equals(b.second);
    }

    public static bool operator !=(pair<T, U> a, pair<T, U> b)
    {
      if (System.Object.ReferenceEquals(a, b)) return false;
      if (((object)a == null) || ((object)b == null)) return true;
      return !a.first.Equals(b.first) || !a.second.Equals(b.second);
    }

  };
}
