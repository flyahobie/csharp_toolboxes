﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace CSharpHelpers
{
  public class ImageFileWriter
  {
    public static void SaveBitmapSource(string filename, BitmapSource bmpSrc)
    {
      string extension = System.IO.Path.GetExtension(filename).ToLower();
      if ((extension == ".jpg") || (extension == ".jpeg"))
      {
        // Creates a JpegBitmapEncoder and adds the BitmapSource to the frames of the encoder
        JpegBitmapEncoder jpegBitmapEncoder = new JpegBitmapEncoder();
        jpegBitmapEncoder.QualityLevel = 97;
        jpegBitmapEncoder.Frames.Add(BitmapFrame.Create(bmpSrc));

        // Saves the image into a file using the encoder
        using (System.IO.Stream stream = System.IO.File.Create(filename))
          jpegBitmapEncoder.Save(stream);
      }
      else if ((extension == ".tif") || (extension == ".tiff"))
      {
        TiffBitmapEncoder tiffBitmapEncoder = new TiffBitmapEncoder();
        tiffBitmapEncoder.Frames.Add(BitmapFrame.Create(bmpSrc));
        // Saves the image into a file using the encoder
        using (System.IO.Stream stream = System.IO.File.Create(filename))
          tiffBitmapEncoder.Save(stream);
      }
      else if (extension == ".png")
      {
        PngBitmapEncoder pngBitmapEncoder = new PngBitmapEncoder();
        pngBitmapEncoder.Frames.Add(BitmapFrame.Create(bmpSrc));
        // Saves the image into a file using the encoder
        using (System.IO.Stream stream = System.IO.File.Create(filename))
          pngBitmapEncoder.Save(stream);
      }
      else if (extension == ".bmp")
      {
        BmpBitmapEncoder bmpBitmapEncoder = new BmpBitmapEncoder();
        bmpBitmapEncoder.Frames.Add(BitmapFrame.Create(bmpSrc));
        // Saves the image into a file using the encoder
        using (System.IO.Stream stream = System.IO.File.Create(filename))
          bmpBitmapEncoder.Save(stream);
      }
    }
  }
}
