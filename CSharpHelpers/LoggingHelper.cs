﻿using System;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;

namespace CSharpHelpers
{
  //=========================================================================================================
  /// <summary>
  /// This class helps create and maintain a small set of log files for the application.
  /// The log files are kept in the user's MyDocuments folder in a subfolder named for the application.
  /// The log file names include their instantiation date. Only the most recent MAX_LOG_FILES are kept.
  /// Older files are automatically deleted as new files are created.
  /// </summary>
  public class LoggingHelper
  {
    //-------------------------------------------------------------------------------------------------------
    protected const int MAX_LOG_FILES = 16;
    protected enum LogType { Info, Warn, Error, ProxyCall, ProxyCallback };

    //-------------------------------------------------------------------------------------------------------
    protected static object critlock = new object();
    protected static LoggingHelper _helper = null;
    protected string _logFilename = null;
    protected Dictionary<string, string> _originatingNames = new Dictionary<string, string>();

    //-------------------------------------------------------------------------------------------------------
    protected LoggingHelper()
    {
      GetLogFilename();
      ClearLog();
    }

    //-------------------------------------------------------------------------------------------------------
    // Provide singleton access to the logging helper
    public static LoggingHelper Instance
    {
      get
      {
        lock (critlock)
        {
          if (_helper == null)
            _helper = new LoggingHelper();
        }

        return _helper;
      }
    }

    #region public methods  //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    public string LogFilename { get { return _logFilename; } }

    //-------------------------------------------------------------------------------------------------------
    public void LogInfo(object originatingObject, string message)
    {
      Log(LogType.Info, originatingObject, message);
    }

    //-------------------------------------------------------------------------------------------------------
    public void LogWarning(object originatingObject, string message)
    {
      Log(LogType.Warn, originatingObject, message);
    }

    //-------------------------------------------------------------------------------------------------------
    public void LogError(object originatingObject, string message)
    {
      Log(LogType.Error, originatingObject, message);
    }

    //-------------------------------------------------------------------------------------------------------
    public void LogProxyCall(object originatingObject, string message)
    {
      Log(LogType.ProxyCall, originatingObject, message);
    }

    //-------------------------------------------------------------------------------------------------------
    public void LogProxyCallback(object originatingObject, string message)
    {
      Log(LogType.ProxyCallback, originatingObject, message);
    }
    //-------------------------------------------------------------------------------------------------------
	// LogginHelper.ThisLineNumber() returns the source code line number IN THE CALLING FUNCTION.
	// Useful for including the source code line number in log messages.
	public static int ThisLineNumber([System.Runtime.CompilerServices.CallerLineNumber] int lineNumber = 0)
	{
		return lineNumber;
	}
    //-------------------------------------------------------------------------------------------------------
	// LogginHelper.ThisFunctionName() returns the name of THE CALLING FUNCTION.  Useful for including the
	// function name log messages.
	public static string ThisFunctionName([System.Runtime.CompilerServices.CallerMemberName] string functionName = "")
	{
		return functionName; 
	}
    //-------------------------------------------------------------------------------------------------------
	// LogginHelper.ThisFilePath() returns the source file path of THE CALLER.  Useful for including the
	// the source code file name in log messages.
	public static string ThisFilePath([System.Runtime.CompilerServices.CallerFilePath] string filePath = "")
	{
		return filePath; 
	}
    #endregion  //-------------------------------------------------------------------------------------------


    #region helper methods  //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------
    private void Log(LogType logType, object originatingObject, string message)
    {
      try
      {
        string nowDate = DateTime.Now.ToShortDateString();
        string nowTime = DateTime.Now.ToLongTimeString();
        string originatingName = GetOriginatingName(originatingObject);

        if (_logFilename == null)
        {
          Debug.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", nowDate, nowTime, logType.ToString(), originatingName, message);
        }
        else
        {
          lock(_logFilename)
          {
            using (FileStream fs = File.OpenWrite(_logFilename))
            {
              using (TextWriter writer = new StreamWriter(fs))
              {

                fs.Seek(0, SeekOrigin.End);
                writer.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", nowDate, nowTime, logType.ToString(), originatingName, message);
                writer.Close();
              }
              fs.Close();
            }
          }
        }
      }
      catch (Exception ex)
      {
        Debug.WriteLine("Exception in LoggingHelper.Log() " + ex.ToString());
      }
    }

    //-------------------------------------------------------------------------------------------------------
    private string GetOriginatingName(object originatingObject)
    {
      string originatingName = "";
      try
      {
        string originatingObjectString = originatingObject.ToString();

        if (!_originatingNames.ContainsKey(originatingObjectString))
        {
          string name = originatingObjectString.Substring(originatingObjectString.LastIndexOf('.') + 1);
          _originatingNames.Add(originatingObjectString, name);
        }

        originatingName = _originatingNames[originatingObjectString];
      }
      catch (Exception ex)
      {
        Debug.WriteLine("Exception in LoggingHelper.GetOriginatingName() " + ex.ToString());
      }
      return originatingName;
    }

    //-------------------------------------------------------------------------------------------------------
    private void ClearLog()
    {

      if (true/*Application.Current.HasElevatedPermissions*/)
      {
        FileInfo fi = new FileInfo(_logFilename);
        fi.Delete();

        // Windows caches creation time (even after a delete, see File System Tunneling).
        // So create the file and set the creation time manually.
        //               
        FileStream fs = fi.Create();
        fs.Close();
        fi.CreationTime = DateTime.Now;
      }
    }

    //-------------------------------------------------------------------------------------------------------
    private void GetLogFilename()
    {
      string applicationName = Application.ResourceAssembly.GetName().Name;
      string baseFilename = applicationName + '_' + DateTime.Now.ToString("yyyyMMdd_HHmmss");
      const string extention = ".log";

      try
      {
        string myDocumentsFolder = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        char dirSepChar = System.IO.Path.DirectorySeparatorChar;
        string logFolder = myDocumentsFolder + dirSepChar + applicationName;
        _logFilename = logFolder + dirSepChar + baseFilename + extention;

        if (!Directory.Exists(logFolder))
          Directory.CreateDirectory(logFolder);

        string logFilePattern = applicationName + "*" + extention;
        IEnumerable<string> fileIterator = Directory.EnumerateFiles(logFolder, logFilePattern, SearchOption.TopDirectoryOnly);
        List<string> filenames = new List<string>(fileIterator);
        filenames.Sort();
        for (int ii = filenames.Count - MAX_LOG_FILES; ii >= 0; --ii)
          File.Delete(filenames[ii]);        
      }
      catch (Exception ex)
      {
        Debug.WriteLine("LoggingHelper: GetLogFilename() error" + ex.ToString());
      }
    }
    #endregion  //-------------------------------------------------------------------------------------------
  }
}
