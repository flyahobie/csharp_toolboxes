// Author: Peter Stubler

#pragma once

#pragma managed(push, off)
#include "opencv2/core.hpp"
#pragma managed(pop)

#using <System.Drawing.dll>
//using namespace System::Drawing;
//using namespace System::Drawing::Imaging;


//----------------------------------------------------------------------------
/// <summary>
/// Copies a cv::Mat to a Bitmap
/// Always returns Format32bppArgb
/// </summary>
/// <param name="mat"></param>
/// <returns>Format32bppArgb Bitmap</returns>
inline System::Drawing::Bitmap ^Mat2Bitmap(const cv::Mat &mat)
{
  int cols = mat.cols;
  int rows = mat.rows;
  //int channels = mat.channels();
  //int type = mat.type();
  //bool cont = mat.isContinuous();
  size_t matStride = mat.step1(0);
  size_t bytesPerPixel = mat.step1(1);

  size_t elemSize = mat.elemSize();          // bytes per pixel
                                             //size_t elemSize1 = mat.elemSize1();      // bytes per color channel (sub-element size)
                                             //const unsigned char *bmpData = mat.data;

  System::Drawing::Bitmap ^bmp = gcnew System::Drawing::Bitmap(cols, rows, System::Drawing::Imaging::PixelFormat::Format32bppArgb);
  System::Drawing::Rectangle rect(0, 0, cols, rows);
  System::Drawing::Imaging::BitmapData ^bmData = bmp->LockBits(rect,  System::Drawing::Imaging::ImageLockMode::ReadWrite, bmp->PixelFormat);
  int bmpStride = bmData->Stride;

  if (elemSize == 1)
  {
    unsigned int *pDst = (unsigned int *)((void *)bmData->Scan0);
    int dstBump = bmpStride / sizeof(unsigned int) - cols;
    for (int yy = 0; yy < rows; ++yy, pDst += dstBump)
    {
      unsigned char *pSrc = (unsigned char *)mat.ptr(yy);
      for (int xx = 0; xx < cols; ++xx)
      {
        unsigned int gray = (unsigned int)*pSrc++;
        unsigned int argb = 0xff000000 | (gray << 16) | (gray << 8) | gray;
        *pDst++ = argb;
      }
    }
  }
  else if (elemSize == 3)
  {
    unsigned int *pDst = (unsigned int *)((void *)bmData->Scan0);
    int dstBump = bmpStride / sizeof(unsigned int) - cols;
    for (int yy = 0; yy < rows; ++yy, pDst += dstBump)
    {
      unsigned char *pSrc = (unsigned char *)mat.ptr(yy);
      for (int xx = 0; xx < cols; ++xx)
      {
        unsigned int blu = (unsigned int)*pSrc++;
        unsigned int grn = (unsigned int)*pSrc++;
        unsigned int red = (unsigned int)*pSrc++;
        unsigned int argb = 0xff000000 | (red << 16) | (grn << 8) | blu;
        *pDst++ = argb;
      }
    }
  }
  else if (elemSize == 4)
  {
    if (bmpStride != matStride)
    {
      unsigned char *pDst = (unsigned char *)((void *)bmData->Scan0);
      int dstBump = bmpStride / sizeof(unsigned int) - cols;
      for (int yy = 0; yy < rows; ++yy, pDst += bmpStride)
        memcpy(pDst, mat.ptr(yy), cols * sizeof(unsigned int));
    }
    else
      memcpy((void *)bmData->Scan0, mat.data, matStride * rows);
  }
  bmp->UnlockBits(bmData);
  return bmp;
}


//----------------------------------------------------------------------------

/// <summary>
/// valid values of Bitmap::PixelFormat { Format24bppRgb, Format32bppRgb, Format32bppArgb }
/// valid values of bytesPerPixel { -1, 1, 3, 4 } 
/// -1  bmp is pseudomonochrome with equivalent RGB values
///  1  bmp is color, convert to grayscale
///  3  bmp is packed RGB
///  4  bmp is ARGB (keep forth channel)
/// </summary>
/// <param name="bmp"></param>
/// <param name="bytesPerPixel"></param>
/// <returns></returns>
inline cv::Mat Bitmap2Mat(System::Drawing::Bitmap ^bmp, int bytesPerPixel)
{
  //if (bmp == nullptr)
  //  return cv::Mat();

  int cols = bmp->Width;
  int rows = bmp->Height;
  System::Drawing::Rectangle rect(0, 0, cols, rows);
  System::Drawing::Imaging::BitmapData ^bmData = bmp->LockBits(rect,  System::Drawing::Imaging::ImageLockMode::ReadWrite, bmp->PixelFormat);
  int bmpStride = bmData->Stride;
  int cvType = (bytesPerPixel == 4) ? CV_8UC4 : (bytesPerPixel == 1) ? CV_8UC1 : CV_8UC3;
  cv::Mat mat(rows, cols, cvType);
  size_t matStride = mat.step1(0);
  if (bmp->PixelFormat == System::Drawing::Imaging::PixelFormat::Format24bppRgb)
  {
    unsigned char *pSrc = (unsigned char *)((void *)bmData->Scan0);
    int srcBump = bmpStride - cols;
    unsigned char *pDst = mat.data;
    switch (bytesPerPixel)
    {
    case -1:
    {
      // assume the mat is actually grayscale with identical channels
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
        for (int xx = 0; xx < cols; ++xx, pSrc += 3)
          *pDst++ = *pSrc;
    }
    break;

    case 1:
    {
      // approximation for 0.299*r + .587*g + .114*b
      unsigned int rscale = 1225; // (int)std::round(0.299 * 4096);
      unsigned int gscale = 2404; // (int)std::round(0.587 * 4096);
      unsigned int bscale = 467;  // (int)std::round(0.114 * 4096);
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
      {
        for (int xx = 0; xx < cols; ++xx)
        {
          unsigned int red = *pSrc++;
          unsigned int grn = *pSrc++;
          unsigned int blu = *pSrc++;
          unsigned int gray_times_4096 = red * rscale + grn * gscale + blu * bscale;
          *pDst++ = (unsigned char)(gray_times_4096 >> 12);
        }
      }
    }
    break;

    case 3:
    {
      for (int yy = 0; yy < rows; ++yy, pSrc += bmpStride, pDst += matStride)
        memcpy(pDst, pSrc, matStride);
    }
    break;

    case 4:
    {
      unsigned int *pDstUint = (unsigned int *)mat.data;
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
      {
        for (int xx = 0; xx < cols; ++xx)
        {
          unsigned int argb = ((unsigned int)*pSrc++ << 16) | ((unsigned int)*pSrc++ << 8) | (unsigned int)*pSrc++;
          *pDstUint++ = argb;
        }
      }
    }
    break;

    }
  }
  else if ((bmp->PixelFormat == System::Drawing::Imaging::PixelFormat::Format32bppRgb) || 
           (bmp->PixelFormat == System::Drawing::Imaging::PixelFormat::Format32bppArgb))
  {
    unsigned int *pSrc = (unsigned int *)((void *)bmData->Scan0);
    int srcBump = bmpStride / sizeof(unsigned int) - cols;
    unsigned char *pDst = mat.data;
    switch (bytesPerPixel)
    {
    case -1:
    {
      // assume the mat is actually grayscale with identical channels
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
      {
        for (int xx = 0; xx < cols; ++xx)
        {
          unsigned int argb = *pSrc++;
          *pDst++ = (unsigned char)(argb & 0xff);
        }
      }
    }
    break;

    case 1:
    {
      // approximation for 0.299*r + .587*g + .114*b
      unsigned int rscale = 1225; // (int)std::round(0.299 * 4096);
      unsigned int gscale = 2404; // (int)std::round(0.587 * 4096);
      unsigned int bscale = 467;  // (int)std::round(0.114 * 4096);
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
      {
        for (int xx = 0; xx < cols; ++xx)
        {
          unsigned int argb = *pSrc++;
          unsigned int gray_times_4096 = ((argb >> 16) & 0xff) * rscale + ((argb >> 8) & 0xff) * gscale + (argb & 0xff) * bscale;
          *pDst++ = (unsigned char)(gray_times_4096 >> 12);
        }
      }
    }
    break;

    case 3:
    {
      for (int yy = 0; yy < rows; ++yy, pSrc += srcBump)
      {
        for (int xx = 0; xx < cols; ++xx)
        {
          unsigned int argb = *pSrc++;
          *pDst++ = (unsigned char)(argb & 0xff);
          *pDst++ = (unsigned char)((argb >> 8) & 0xff);
          *pDst++ = (unsigned char)((argb >> 16) & 0xff);
        }
      }
    }
    break;

    case 4:
    {
      unsigned char *pSrcByte = (unsigned char *)((void *)bmData->Scan0);
      unsigned char *pDstByte = mat.data;
      for (int yy = 0; yy < rows; ++yy, pSrcByte += bmpStride, pDstByte += matStride)
        memcpy(pDstByte, pSrcByte, matStride);
    }
    break;

    }
  }
  bmp->UnlockBits(bmData);
  return mat;
}

//----------------------------------------------------------------------------
// default to RGB pixel interleaved packed
inline cv::Mat Bitmap2Mat(System::Drawing::Bitmap ^bmp) { return Bitmap2Mat(bmp, 3); } 


