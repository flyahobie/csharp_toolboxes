// Author: Peter Stubler

#pragma once
#pragma managed(push, off)
#include "opencv2/core.hpp"
#pragma managed(pop)

#using <PresentationCore.dll>
//using namespace System::Windows::Media::Imaging;
#using <WindowsBase.dll>
//using namespace System::Windows::Media;


//-----------------------------------------------------------------------------
/// <summary>
/// Copies a cv::Mat to a BitmapSource
/// </summary>
/// <param name="mat"></param>
/// <returns></returns>
inline System::Windows::Media::Imaging::BitmapSource ^Mat2BitmapSource(const cv::Mat &mat)
{
  int cols = mat.cols;
  int rows = mat.rows;
  int cvType = mat.type();
  int matStride = (int)mat.step1(0);
  int matSize = matStride * rows;
  System::IntPtr pData = (System::IntPtr)mat.data;
  double dpi = 96;

  System::Windows::Media::Imaging::BitmapSource ^bmpSrc = nullptr;
  if ((cols > 0) && (rows > 0))
  {
    if (cvType == CV_8UC1)
      bmpSrc = System::Windows::Media::Imaging::BitmapSource::Create(cols, rows, dpi, dpi, System::Windows::Media::PixelFormats::Gray8, nullptr, pData, matSize, matStride);
    else if (cvType == CV_8UC3)
      bmpSrc = System::Windows::Media::Imaging::BitmapSource::Create(cols, rows, dpi, dpi, System::Windows::Media::PixelFormats::Bgr24, nullptr, pData, matSize, matStride);
    else if (cvType == CV_8UC4)
      bmpSrc = System::Windows::Media::Imaging::BitmapSource::Create(cols, rows, dpi, dpi, System::Windows::Media::PixelFormats::Bgra32, nullptr, pData, matSize, matStride);
  }
  return bmpSrc;
}

//-----------------------------------------------------------------------------
/// <summary>
/// Convert a BitmapSource to a cv::Mat
/// </summary>
/// <param name="bmpSrc"></param>
/// <returns></returns>
inline cv::Mat BitmapSource2Mat(System::Windows::Media::Imaging::BitmapSource ^bmpSrc)
{
  int cols = bmpSrc->PixelWidth;
  int rows = bmpSrc->PixelHeight;
  int cvType;
  if (bmpSrc->Format == System::Windows::Media::PixelFormats::Gray8)
    cvType = CV_8UC1;
  else if (bmpSrc->Format == System::Windows::Media::PixelFormats::Bgr24)
    cvType = CV_8UC3;
  else if ((bmpSrc->Format == System::Windows::Media::PixelFormats::Bgr32) || (bmpSrc->Format == System::Windows::Media::PixelFormats::Bgra32))
    cvType = CV_8UC4;
  else
    return cv::Mat();
  cv::Mat mat(rows, cols, cvType);
  int matStride = (int)mat.step1(0); // step1(0) ==> bytes per row / bytes per pixel
  int matSize = matStride * rows;
  if ((cols > 0) && (rows > 0))
  {
    System::Windows::Int32Rect sourceRect(0, 0, cols, rows);
    bmpSrc->CopyPixels(sourceRect, (System::IntPtr)mat.data, matSize, matStride);
  }
  return mat;
}

