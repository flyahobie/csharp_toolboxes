// Author: Peter Stubler

#pragma once

#pragma managed(push, off)
#include "opencv2/core.hpp"
#pragma managed(pop)

#using <WindowsBase.dll>
#using <PresentationCore.dll>
//using namespace System::Windows;

//-----------------------------------------------------------------------------------------------------------
/// <summary>
/// Copy an OpenCV cv::Mat to a two dimensional CLI array
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="mat"></param>
/// <returns></returns>
template <typename T>
cli::array<T,2> ^Mat2Array(const cv::Mat &mat)
{
  cli::array<T,2> ^mArr = gcnew cli::array<T,2>(mat.rows, mat.cols);

  for (int rr = 0; rr < mat.rows; ++rr)
  {
    const T *row = mat.ptr<T>(rr);
    for (int cc = 0; cc < mat.cols; ++cc)
      mArr[rr, cc] = row[cc];
  }

  return mArr;
}

//-----------------------------------------------------------------------------------------------------------
/// <summary>
/// Copy a two dimensional CLI array to an OpenCV cv::Mat
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="mArr"></param>
/// <returns></returns>
template <typename T>
cv::Mat Array2Mat(cli::array<T,2> ^mArr)
{
  int rows = mArr->GetLength(0);
  int cols = mArr->GetLength(1);
  int cvType = 0;
  if (T::typeid == float::typeid)
    cvType = CV_32F;
  else if (T::typeid == double::typeid)
    cvType = CV_64F;
  else
    throw std::exception();

  cv::Mat mat(rows, cols, cvType);
  for (int rr = 0; rr < rows; ++rr)
  {
    T *row = mat.ptr<T>(rr);
    for (int cc = 0; cc < cols; ++cc)
      row[cc] = mArr[rr, cc];
  }

  return mat;
}
