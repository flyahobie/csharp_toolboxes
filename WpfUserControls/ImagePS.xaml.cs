﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;

using System.Windows.Interop;
using System.Windows.Markup;
using System.IO;

namespace WpfUserControls
{
  /// <summary>
  /// Interaction logic for ImagePS.xaml
  /// </summary>
  public partial class ImagePS : UserControl
  {
    //==================================================================================================
    /// <summary>
    /// Options available after selecting a portion of the image
    /// </summary>
    public enum DragModes
    {
      /// <summary>
      /// no effect on the image or scroll bars
      /// </summary>
      None = 0,
      /// <summary>
      /// pan the image by adjusting scroll bars
      /// </summary>
      Pan,
      /// <summary>
      /// Select the image region
      /// </summary>
      Select,
      /// <summary>
      /// Zoom in on the image
      /// </summary>
      Zoom
    }

    //-------------------------------------------------------------------------------------------------------
    protected BitmapSource displayBMI = null;
    protected BitmapSource overlayBMI = null;
    protected Point mouseDownScrollOffset;
    protected Point mouseDownPosition;
    protected Point hackSelectGridMouseDownPosition;
    protected Point previousDisplayCenterPosition = new Point();
    protected bool temporaryPanMode = false;
    protected bool zoomDragging = false;
    protected Cursor cursorSelectCross;
    protected Cursor cursorMoveSelect;
    protected Cursor cursorHand;
    protected Cursor cursorGrab;
    protected Cursor cursorZoomIn;
    protected Cursor cursorZoomOut;
    protected Point displayOne2oneScale;
    protected Point overlayOne2oneScale;
    protected Point windowsScreenDPI = new Point(96.0, 96.0); // Yup! a magic number
    //protected double currentScale = 1;
    protected double[] standardScales = { 16, 12, 8, 7, 6, 5, 4, 3, 2, 1, 2D/3, 1D/2, 
                                          1D/3, 1D/4, 1D/6, 1D/8, 1D/12, 1D/16, 1D/20, 
                                          1D/25, 1D/30, 1D/50, 3D/200, 1D/100, 1D/150, 
                                          1D/200, 1D/250, 1D/300, 1D/500, 1D/1000, 
                                          1D/2000, 1D/4000, 1D/5000 };

    //-------------------------------------------------------------------------------------------------------
    public ImagePS()
    {
      InitializeComponent();
      this.Loaded += new RoutedEventHandler(ImagePS_Loaded);

      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.SelectCross))
        cursorSelectCross = new Cursor(cursorMemoryStream);
      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.MoveSelect))
        cursorMoveSelect = new Cursor(cursorMemoryStream);
      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.HandOpen))
        cursorHand = new Cursor(cursorMemoryStream);
      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.HandGrab))
        cursorGrab = new Cursor(cursorMemoryStream);
      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.ZoomIn))
        cursorZoomIn = new Cursor(cursorMemoryStream);
      using (System.IO.MemoryStream cursorMemoryStream = new System.IO.MemoryStream(Properties.Resources.ZoomOut))
        cursorZoomOut = new Cursor(cursorMemoryStream);

      CurrentScale = 1;
      DragMode = DragModes.None;
      UserItems = new ObservableCollection<UIElement>();
    }

    //-------------------------------------------------------------------------------------------------------
    void ImagePS_Loaded(object sender, RoutedEventArgs e)
    {
      // 96 is the "normal" dpi
#if false 
      var source = PresentationSource.FromVisual(this);
      if (source != null)
      {
        windowsScreenDPI.X = 96.0 * source.CompositionTarget.TransformToDevice.M11;
        windowsScreenDPI.Y = 96.0 * source.CompositionTarget.TransformToDevice.M22;
      }
#endif
      ScaleToFit();
      UpdateDisplaySelectRect();
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty DebugTextProperty = 
      DependencyProperty.Register("DebugText", typeof(string), typeof(ImagePS));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public string DebugText
    {
      get { return (string)GetValue(DebugTextProperty); }
      set { SetValue(DebugTextProperty, value); }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Gets or sets the drag options
    /// </summary>
    [Category("Behavior"),
    DefaultValue(ImagePS.DragModes.None),
    Description("Controls the drag behavior. (None, Pan, Select, Zoom)"),
    Browsable(true)]
    public static readonly DependencyProperty DragModeProperty =
      DependencyProperty.Register("DragMode", typeof(DragModes), typeof(ImagePS), new PropertyMetadata(OnDragModePropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public DragModes DragMode
    {
      get { return (DragModes)GetValue(DragModeProperty); }
      set { SetValue(DragModeProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnDragModePropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      DragModes dragMode = (DragModes)e.NewValue;
      if (dragMode == DragModes.None)
        thisImagePS.SelectRect = Int32Rect.Empty;
      thisImagePS.BestCursor();
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty SourceProperty =
      DependencyProperty.Register("Source", typeof(ImageSource), typeof(ImagePS), new PropertyMetadata(OnSourcePropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public ImageSource Source
    {
      get { return (ImageSource)GetValue(SourceProperty); }
      set { SetValue(SourceProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnSourcePropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      (target as ImagePS).SetDisplayImage(e.NewValue as BitmapSource);
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty SourceOpacityProperty =
      DependencyProperty.Register("SourceOpacity", typeof(double), typeof(ImagePS), new PropertyMetadata(OnSourceOpacityPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public double SourceOpacity
    {
      get { return (double)GetValue(SourceOpacityProperty); }
      set { SetValue(SourceOpacityProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnSourceOpacityPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.displayImage.Opacity = thisImagePS.SourceOpacity;
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty OverlaySourceProperty =
      DependencyProperty.Register("OverlaySource", typeof(ImageSource), typeof(ImagePS), new PropertyMetadata(OnOverlaySourcePropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public ImageSource OverlaySource
    {
      get { return (ImageSource)GetValue(OverlaySourceProperty); }
      set { SetValue(OverlaySourceProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnOverlaySourcePropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      (target as ImagePS).SetOverlayImage(e.NewValue as BitmapSource);
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty OverlayOpacityProperty =
      DependencyProperty.Register("OverlayOpacity", typeof(double), typeof(ImagePS), new PropertyMetadata(OnOverlayOpacityPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public double OverlayOpacity
    {
      get { return (double)GetValue(OverlayOpacityProperty); }
      set { SetValue(OverlayOpacityProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnOverlayOpacityPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.overlayImage.Opacity = thisImagePS.OverlayOpacity;
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty OverlayVisibilityProperty =
      DependencyProperty.Register("OverlayVisibility", typeof(Visibility), typeof(ImagePS), new PropertyMetadata(OnOverlayVisibilityPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public Visibility OverlayVisibility
    {
      get { return (Visibility)GetValue(OverlayVisibilityProperty); }
      set { SetValue(OverlayVisibilityProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnOverlayVisibilityPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.overlayImage.Visibility = thisImagePS.OverlayVisibility;
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Gets or sets the Image rendering options
    /// </summary>
    [Category("Behavior"),
    DefaultValue(BitmapScalingMode.Fant),
    Description("Sets the rendering method for the displayed image."),
    Browsable(true)]
    public BitmapScalingMode OverlayBitmapScalingMode
    {
      get { return RenderOptions.GetBitmapScalingMode(overlayImage); }
      set { RenderOptions.SetBitmapScalingMode(overlayImage, value); }
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty UserItemsProperty =
      DependencyProperty.Register("UserItems", typeof(ObservableCollection<UIElement>), typeof(ImagePS), new PropertyMetadata(OnUserItemsPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public ObservableCollection<UIElement> UserItems
    {
      get { return (ObservableCollection<UIElement>)GetValue(UserItemsProperty); }
      set { SetValue(UserItemsProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnUserItemsPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      if (thisImagePS != null)
      {
        thisImagePS.SetUserItemsCollectionChangedEventHandler();
        thisImagePS.userItems_CollectionChanged(e.NewValue, new System.Collections.Specialized.NotifyCollectionChangedEventArgs(System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
      }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public void SetUserItemsCollectionChangedEventHandler()
    {
      if (UserItems != null)
        UserItems.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(userItems_CollectionChanged);
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public void userItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      ObservableCollection<UIElement> collection = sender as ObservableCollection<UIElement>;

      if (collection != null)
      { 
        foreach (UIElement element in collection)
          if (!userCanvas.Children.Contains(element))
            userCanvas.Children.Add(element);

        List<UIElement> removeList = new List<UIElement>();
        foreach (UIElement element in userCanvas.Children)
          if (!collection.Contains(element))
            removeList.Add(element);

        foreach (UIElement element in removeList)
          userCanvas.Children.Remove(element);
      }
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty UserItemsOpacityProperty =
      DependencyProperty.Register("UserItemsOpacity", typeof(double), typeof(ImagePS), new PropertyMetadata(OnUserItemsOpacityPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public double UserItemsOpacity
    {
      get { return (double)GetValue(UserItemsOpacityProperty); }
      set { SetValue(UserItemsOpacityProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnUserItemsOpacityPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.userCanvas.Opacity = thisImagePS.UserItemsOpacity;
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty UserItemsVisibilityProperty =
      DependencyProperty.Register("UserItemsVisibility", typeof(Visibility), typeof(ImagePS), new PropertyMetadata(OnUserItemsVisibilityPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public Visibility UserItemsVisibility
    {
      get { return (Visibility)GetValue(UserItemsVisibilityProperty); }
      set { SetValue(UserItemsVisibilityProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnUserItemsVisibilityPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.userCanvas.Visibility = thisImagePS.UserItemsVisibility;
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty CanvasSizeProperty =
      DependencyProperty.Register("CanvasSize", typeof(Size), typeof(ImagePS));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public Size CanvasSize
    {
      get { return (Size)GetValue(CanvasSizeProperty); }
      set { SetValue(CanvasSizeProperty, value); }
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty SelectionAspectRatioProperty =
      DependencyProperty.Register("SelectionAspectRatio", typeof(Size), typeof(ImagePS), new PropertyMetadata(OnSelectionAspectRatioPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public Size SelectionAspectRatio
    {
      get { return (Size)GetValue(SelectionAspectRatioProperty); }
      set { SetValue(SelectionAspectRatioProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnSelectionAspectRatioPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      if ((thisImagePS.SelectionAspectRatio.Width > 0) && (thisImagePS.SelectionAspectRatio.Height > 0) && !thisImagePS.SelectRect.IsEmpty)
      {
        double scaleX = thisImagePS.SelectRect.Width / thisImagePS.SelectionAspectRatio.Width;
        double scaleY = thisImagePS.SelectRect.Height / thisImagePS.SelectionAspectRatio.Height;
        if (scaleX > scaleY)
        {
          int width = (int)Math.Round(thisImagePS.SelectRect.Height * thisImagePS.SelectionAspectRatio.Width / thisImagePS.SelectionAspectRatio.Height);
          int x = (int)Math.Round(thisImagePS.SelectRect.X + (thisImagePS.SelectRect.Width - width) / 2.0);
          thisImagePS.SelectRect = new Int32Rect(x, thisImagePS.SelectRect.Y, width, thisImagePS.SelectRect.Height);
        }
        else
        {
          int height = (int)Math.Round(thisImagePS.SelectRect.Width * thisImagePS.SelectionAspectRatio.Height / thisImagePS.SelectionAspectRatio.Width);
          int y = (int)Math.Round(thisImagePS.SelectRect.Y + (thisImagePS.SelectRect.Height - height) / 2.0);
          thisImagePS.SelectRect = new Int32Rect(thisImagePS.SelectRect.X, y, thisImagePS.SelectRect.Width, height);
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Sets the Image 
    /// </summary>
    public void SetDisplayImage(BitmapSource bitmapSource)
    {
      bool scaleToFit = (bitmapSource != null) && ((displayBMI == null) || (displayBMI.PixelWidth != bitmapSource.PixelWidth) || (displayBMI.PixelHeight != bitmapSource.PixelHeight));
      displayBMI =  bitmapSource;
      displayImage.Source = displayBMI;
      SelectRect = Int32Rect.Empty;
      if (displayBMI == null)
      {
        userCanvas.Width = 0;
        userCanvas.Height = 0;
        displayOne2oneScale.X = 1;
        displayOne2oneScale.Y = 1;
        CurrentScale = 1;
      }
      else
      {
        Point pos = CurrentDisplayCenterPosition;
        double scale = CurrentScale;
        userCanvas.Width = displayBMI.PixelWidth;
        userCanvas.Height = displayBMI.PixelHeight;
        displayOne2oneScale.X = (displayBMI.DpiX == 0) ? 1 : displayBMI.DpiX / windowsScreenDPI.X;
        displayOne2oneScale.Y = (displayBMI.DpiY == 0) ? 1 : displayBMI.DpiY / windowsScreenDPI.Y;
        if (overlayBMI != null)
        {
          // assume we want to overlay the images
          overlayOne2oneScale.X = (overlayBMI.DpiX / windowsScreenDPI.X) * ((double)displayBMI.PixelWidth / overlayBMI.PixelWidth);
          overlayOne2oneScale.Y = (overlayBMI.DpiY / windowsScreenDPI.Y) * ((double)displayBMI.PixelHeight / overlayBMI.PixelHeight);
        }
        if (scaleToFit)
          ScaleToFit();
        else
        {
          CurrentScale = 0;
          Zoom(scale, pos);
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Sets the Overlay Image (unlike SetImage, this will not affect the CurrentScale or current position
    /// </summary>
    /// <param name="bitmapSource"></param>
    public void SetOverlayImage(BitmapSource bitmapSource)
    {
      overlayBMI = bitmapSource;
      overlayImage.Source = bitmapSource;
      if (overlayBMI == null)
      {
        overlayOne2oneScale.X = 1;
        overlayOne2oneScale.Y = 1;
      }
      else if (displayBMI == null)
      {
        overlayOne2oneScale.X = (overlayBMI.DpiX == 0) ? 1 : overlayBMI.DpiX / windowsScreenDPI.X;
        overlayOne2oneScale.Y = (overlayBMI.DpiY == 0) ? 1 : overlayBMI.DpiY / windowsScreenDPI.Y;
      }
      else // ((displayBMI != null) && (overlayBMI != null))
      {
        // assume we want to overlay the images
        overlayOne2oneScale.X = (overlayBMI.DpiX / windowsScreenDPI.X) * ((double)displayBMI.PixelWidth / overlayBMI.PixelWidth);
        overlayOne2oneScale.Y = (overlayBMI.DpiY / windowsScreenDPI.Y) * ((double)displayBMI.PixelHeight / overlayBMI.PixelHeight);
      }
#if false
      Matrix o = overlayImage.LayoutTransform.Value;
      o.M11 = overlayOne2oneScale.X * CurrentScale;
      o.M22 = overlayOne2oneScale.Y * CurrentScale;
      overlayImage.LayoutTransform = new MatrixTransform(o);
      this.UpdateLayout();
#else
      OnCurrentScaleChanged(CurrentScale, CurrentScale);
#endif
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// If the pixel dimensions are equivalent, sets the Image for the displayImage without altering the viewing rectangle
    /// </summary>
    public void ReplaceImage(BitmapSource replacement)
    {
      if ((replacement != null) && (displayBMI != null) && 
          (replacement.PixelWidth == displayBMI.PixelWidth) &&
          (replacement.PixelHeight == displayBMI.PixelHeight))
      {
        displayBMI = replacement;
        displayImage.Source = displayBMI;
      }
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty SelectRectProperty =
      DependencyProperty.Register("SelectRect", typeof(Int32Rect), typeof(ImagePS), new PropertyMetadata(OnSelectRectPropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public Int32Rect SelectRect
    {
      get { return (Int32Rect)GetValue(SelectRectProperty); }
      set { SetValue(SelectRectProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public event EventHandler SelectRectChanged;

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnSelectRectPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS thisImagePS = target as ImagePS;
      thisImagePS.UpdateDisplaySelectRect();
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public void UpdateDisplaySelectRect()
    {
      Int32Rect clipRect = ClipRectToImage(SelectRect);
      if (clipRect.IsEmpty)
        selectRectangle.Visibility = Visibility.Hidden;
      else if (displayBMI != null)
      {
        int left = Math.Max(0, Math.Min(clipRect.X, displayBMI.PixelWidth - 1));
        int top = Math.Max(0, Math.Min(clipRect.Y, displayBMI.PixelHeight - 1));
        int right = Math.Min(clipRect.X + clipRect.Width, displayBMI.PixelWidth);
        int bottom = Math.Min(clipRect.Y + clipRect.Height, displayBMI.PixelHeight);
        Point topLeft = displayImage.TranslatePoint(new Point(left / displayOne2oneScale.X, top / displayOne2oneScale.Y), selectionCanvas);
        Canvas.SetLeft(selectRectangle, topLeft.X);
        Canvas.SetTop(selectRectangle, topLeft.Y);
        double scale = (CurrentScale == 0) ? 1 : CurrentScale;
        selectRectangle.Width = (right - left) * scale;
        selectRectangle.Height = (bottom - top) * scale;
        selectRectangle.Visibility = Visibility.Visible;
      }
      if (SelectRectChanged != null)
        SelectRectChanged(this, new EventArgs());
    }

    //-------------------------------------------------------------------------------------------------------
    public void SelectAll()
    {
      // Selects whole image except when the SelectionAspectRatio is valid.
      // Then, it selects the larges central portion of the image that
      // meets the SelectionAspectRatio requirements.
      if (displayBMI != null)
      {
        if ((SelectionAspectRatio.Width == 0) || SelectionAspectRatio.Height == 0)
          SelectRect = new Int32Rect(0, 0, displayBMI.PixelWidth, displayBMI.PixelHeight);
        else
        {
          double scaleX = displayBMI.PixelWidth / SelectionAspectRatio.Width;
          double scaleY = displayBMI.PixelHeight / SelectionAspectRatio.Height;
          if (scaleX > scaleY)
          {
            int width = Math.Min((int)Math.Round(displayBMI.PixelHeight * SelectionAspectRatio.Width / SelectionAspectRatio.Height), displayBMI.PixelWidth);
            SelectRect = new Int32Rect((displayBMI.PixelWidth - width) / 2, 0, width, displayBMI.PixelHeight);
          }
          else
          {
            int height = Math.Min((int)Math.Round(displayBMI.PixelWidth * SelectionAspectRatio.Height / SelectionAspectRatio.Width), displayBMI.PixelHeight);
            SelectRect = new Int32Rect(0, (displayBMI.PixelHeight - height) / 2, displayBMI.PixelWidth, height);
          }
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// Gets or sets the Image rendering options
    /// </summary>
    [Category("Behavior"),
    DefaultValue(BitmapScalingMode.Fant),
    Description("Sets the rendering method for the displayed image."),
    Browsable(true)]
    public BitmapScalingMode BitmapScalingMode
    {
      get { return RenderOptions.GetBitmapScalingMode(displayImage); }
      set { RenderOptions.SetBitmapScalingMode(displayImage, value); }
    }

    //-------------------------------------------------------------------------------------------------------
    public static readonly DependencyProperty CurrentScaleProperty =
      DependencyProperty.Register("CurrentScale", typeof(double), typeof(ImagePS), new PropertyMetadata(OnCurrentScalePropertyChanged));

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public double CurrentScale
    {
      get { return (double)GetValue(CurrentScaleProperty); }
      protected set { SetValue(CurrentScaleProperty, value); }
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    public event EventHandler CurrentScaleChanged;

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected static void OnCurrentScalePropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
    {
      ImagePS imagePS = target as ImagePS;
      imagePS.OnCurrentScaleChanged((double)e.OldValue, (double)e.NewValue);
    }

    // . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    protected void OnCurrentScaleChanged(double oldScale, double newScale)
    {
      // select rectangle
      if (oldScale == 0)
        oldScale = 1;
      double relScale = newScale / oldScale;
      Canvas.SetTop(selectRectangle, Canvas.GetTop(selectRectangle) * relScale);
      Canvas.SetLeft(selectRectangle, Canvas.GetLeft(selectRectangle) * relScale);
      selectRectangle.Width = selectRectangle.Width * relScale;
      selectRectangle.Height = selectRectangle.Height * relScale;

      // zoom rectangle
      zoomRectangle.Visibility = Visibility.Hidden;

      // the image
      //Matrix m = displayImage.RenderTransform.Value;
      //m.M11 = displayOne2oneScale.X * newScale;
      //m.M22 = displayOne2oneScale.Y * newScale;
      //displayImage.RenderTransform = new MatrixTransform(m);

      Matrix m = displayImage.LayoutTransform.Value;
      m.M11 = displayOne2oneScale.X * newScale;
      m.M22 = displayOne2oneScale.Y * newScale;
      displayImage.LayoutTransform = new MatrixTransform(m);

      Matrix o = overlayImage.LayoutTransform.Value;
      o.M11 = overlayOne2oneScale.X * newScale;
      o.M22 = overlayOne2oneScale.Y * newScale;
      overlayImage.LayoutTransform = new MatrixTransform(o);

      Matrix u = displayImage.LayoutTransform.Value;
      u.M11 = newScale;
      u.M22 = newScale;
      userCanvas.LayoutTransform = new MatrixTransform(u);
      this.UpdateLayout();

      if (CurrentScaleChanged != null)
        CurrentScaleChanged(this, new EventArgs());
    }

    //-------------------------------------------------------------------------------------------------------
    public void ScaleToFit()
    {
      if (displayBMI != null)
      {
        double scaleX = scrollViewer.ViewportWidth / displayBMI.PixelWidth;
        double scaleY = scrollViewer.ViewportHeight / displayBMI.PixelHeight;
        CurrentScale = Math.Min(scaleX, scaleY);
      }
    }

    //-------------------------------------------------------------------------------------------------------
    public void Zoom(double scale, Point loc)
    {
      CurrentScale = scale;
      // figure out the ideal  offset to center the image on loc
      double xoff = loc.X * scale - scrollViewer.ViewportWidth / 2;
      double yoff = loc.Y * scale - scrollViewer.ViewportHeight / 2;
      scrollViewer.ScrollToHorizontalOffset(xoff);
      scrollViewer.ScrollToVerticalOffset(yoff);
    }

    //-------------------------------------------------------------------------------------------------------
    public double NextZoomInScale()
    {
      int idx = 0;
      for (; idx + 1 < standardScales.Length; ++idx)
        if (standardScales[idx + 1] <= CurrentScale)
          break;

      return standardScales[idx];
    }

    //-------------------------------------------------------------------------------------------------------
    public double NextZoomOutScale()
    {
      int idx = standardScales.Length - 1;
      for (; idx - 1 > 0; --idx)
        if (standardScales[idx - 1] >= CurrentScale)
          break;

      return standardScales[idx];
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnKeyDown(KeyEventArgs e)
    {
      base.OnKeyDown(e);
      bool altDown = Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
      if (Mouse.LeftButton == MouseButtonState.Released)
      {
        if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
        {
          if (e.Key == Key.OemPlus)
            Zoom(NextZoomInScale(), CurrentDisplayCenterPosition);
          else if (e.Key == Key.OemMinus)
            Zoom(NextZoomOutScale(), CurrentDisplayCenterPosition);
        }
        else if ((DragMode == DragModes.Zoom) && altDown)
          Cursor = cursorZoomOut;
        else if (Keyboard.IsKeyDown(Key.Space))
          Cursor = cursorHand;
        e.Handled = true;
      }
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnKeyUp(KeyEventArgs e)
    {
      base.OnKeyUp(e);
      BestCursor();
      e.Handled = true;   
    }

    //-------------------------------------------------------------------------------------------------------
    public Point Grid2Image(Point p)
    {
      return Round(FixImageScale(TranslatePoint(p, displayImage)));
    }

    //-------------------------------------------------------------------------------------------------------
    public Point FixImageScale(Point p)
    {
      return new Point(p.X * displayOne2oneScale.X, p.Y * displayOne2oneScale.Y);
    }

    //-------------------------------------------------------------------------------------------------------
    public static Point Round(Point p, int digits = 0)
    {
      return new Point(Math.Round(p.X, digits), Math.Round(p.Y, digits));
    }

    //-------------------------------------------------------------------------------------------------------
    public Point Image2Grid(Point p)
    {
      return displayImage.TranslatePoint(new Point(p.X / displayOne2oneScale.X, p.Y / displayOne2oneScale.Y), this);
    }

    //-------------------------------------------------------------------------------------------------------
    public Point CurrentDisplayCenterPosition
    {
      get { return Grid2Image(new Point(scrollViewer.ViewportWidth / 2, scrollViewer.ViewportHeight / 2)); }
    }

    //-------------------------------------------------------------------------------------------------------
    public event EventHandler CurrentDisplayCenterPositionChanged;

    //-------------------------------------------------------------------------------------------------------
    private void scrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
    {
      if (CurrentDisplayCenterPositionChanged != null)
      {
        Point current = CurrentDisplayCenterPosition;
        if (current != previousDisplayCenterPosition)
        {
          CurrentDisplayCenterPositionChanged(this, new EventArgs());
          previousDisplayCenterPosition = current;
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnMouseEnter(MouseEventArgs e)
    {
      base.OnMouseEnter(e);
      BestCursor();
      Focus();
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
    {
      base.OnPreviewMouseDown(e);
      //if (displayImage.IsMouseOver)
      if (displayGrid.IsMouseOver)
      {
        mouseDownPosition = e.GetPosition(this);
        mouseDownScrollOffset = new Point(scrollViewer.HorizontalOffset, scrollViewer.VerticalOffset);

        if (Keyboard.IsKeyDown(Key.Space) || (DragMode == DragModes.Pan))
        {
          temporaryPanMode = true;
          Cursor = cursorGrab;
        }
        if (Cursor == cursorMoveSelect)
          hackSelectGridMouseDownPosition = new Point(Canvas.GetLeft(selectRectangle), Canvas.GetTop(selectRectangle));

        if (!displayGrid.IsMouseCaptured)
          displayGrid.CaptureMouse();
      }
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnPreviewMouseMove(MouseEventArgs e)
    {
      base.OnPreviewMouseMove(e);
      Point currentPosition = e.GetPosition(this);
      bool inSelectRect = false;
      if (displayGrid.IsMouseCaptured)
      {
        if (temporaryPanMode || (DragMode == DragModes.Pan))
        {
          scrollViewer.ScrollToHorizontalOffset(mouseDownScrollOffset.X - (currentPosition.X - mouseDownPosition.X));
          scrollViewer.ScrollToVerticalOffset(mouseDownScrollOffset.Y - (currentPosition.Y - mouseDownPosition.Y));
        }
        else if ((DragMode == DragModes.Select) && (displayBMI != null))
        {
          // Round to image pixel boundaries then translate to overlay
          Point overlayMouseDownPosition = TranslatePoint(Image2Grid(Grid2Image(mouseDownPosition)), selectionCanvas);
          Point overlayCurrentPosition = TranslatePoint(Image2Grid(Grid2Image(currentPosition)), selectionCanvas);

          Rect rect = Rect.Empty;
          if (Cursor == cursorMoveSelect)
          {
            double left = hackSelectGridMouseDownPosition.X + currentPosition.X - mouseDownPosition.X;
            double top = hackSelectGridMouseDownPosition.Y + currentPosition.Y - mouseDownPosition.Y;
            left = Math.Max(0, Math.Min(left, selectionCanvas.ActualWidth - selectRectangle.Width));
            top = Math.Max(0, Math.Min(top, selectionCanvas.ActualHeight - selectRectangle.Height));
            Canvas.SetLeft(selectRectangle, left);
            Canvas.SetTop(selectRectangle, top);
            //Point leftTop = Image2Grid(new Point(SelectRect.X, SelectRect.Y));
            //rect = new Rect(leftTop.X + (currentPosition.X - mouseDownPosition.X),
            //                leftTop.Y + (currentPosition.Y - mouseDownPosition.Y),
            //                SelectRect.Width * CurrentScale,
            //                SelectRect.Height * CurrentScale);
          }
          else if (Cursor == cursorSelectCross)
          {
            // clip to bounds
            overlayCurrentPosition.X = Math.Max(0, Math.Min(overlayCurrentPosition.X, selectionCanvas.ActualWidth));
            overlayCurrentPosition.Y = Math.Max(0, Math.Min(overlayCurrentPosition.Y, selectionCanvas.ActualHeight));
            if ((SelectionAspectRatio.Width > 0) && (SelectionAspectRatio.Height > 0))
            {
              double width = Math.Abs(overlayCurrentPosition.X - overlayMouseDownPosition.X);
              double height = Math.Abs(overlayCurrentPosition.Y - overlayMouseDownPosition.Y);
              if ((width != 0) && (height != 0))
              {
                double scaleX = width / SelectionAspectRatio.Width;
                double scaleY = height / SelectionAspectRatio.Height;
                if (scaleX > scaleY)
                {
                  double sign = overlayCurrentPosition.X > overlayMouseDownPosition.X ? 1 : -1;
                  overlayCurrentPosition.X = overlayMouseDownPosition.X + sign * height * SelectionAspectRatio.Width / SelectionAspectRatio.Height;
                }
                else
                {
                  double sign = overlayCurrentPosition.Y > overlayMouseDownPosition.Y ? 1 : -1;
                  overlayCurrentPosition.Y = overlayMouseDownPosition.Y + sign * width * SelectionAspectRatio.Height / SelectionAspectRatio.Width;
                }
              }
            }
            rect = GenerateRect(overlayMouseDownPosition, overlayCurrentPosition);
            // Commented out region is nice for symmetrical regions, but clipping it properly would be a pain.
            //bool altDown = Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
            //if (altDown)
            //{
            //  rect.X = overlayMouseDownPosition.X - rect.Width;
            //  rect.Width *= 2;
            //  rect.Y = overlayMouseDownPosition.Y - rect.Height;
            //  rect.Height *= 2;
            //}

            // display
            Canvas.SetTop(selectRectangle, rect.Top);
            Canvas.SetLeft(selectRectangle, rect.Left);
            selectRectangle.Width = rect.Width;
            selectRectangle.Height = rect.Height;
            selectRectangle.Visibility = Visibility.Visible;
          }
        }
        else if (DragMode == DragModes.Zoom)
        {
          Point overlayMouseDownPosition = TranslatePoint(mouseDownPosition, selectionCanvas);
          Point overlayCurrentPosition = TranslatePoint(currentPosition, selectionCanvas);
          Rect rect = GenerateRect(overlayMouseDownPosition, overlayCurrentPosition);
          Canvas.SetTop(zoomRectangle, rect.Top);
          Canvas.SetLeft(zoomRectangle, rect.Left);
          zoomRectangle.Width = rect.Width;
          zoomRectangle.Height = rect.Height;
          zoomRectangle.Visibility = Visibility.Visible;
        }
      }
      else if ((DragMode == DragModes.Select) && !SelectRect.IsEmpty)
      {
        Point imgPos = Grid2Image(currentPosition);
        inSelectRect = (imgPos.X >= SelectRect.X) && (imgPos.X < (SelectRect.X + SelectRect.Width))
                         && (imgPos.Y >= SelectRect.Y) && (imgPos.Y < (SelectRect.Y + SelectRect.Height));
        if (inSelectRect && (Cursor != cursorMoveSelect))
          Cursor = cursorMoveSelect;
        else if (!inSelectRect && (Cursor != cursorSelectCross))
          Cursor = cursorSelectCross;
      }

#if DEBUG
      Point imagePosition = Grid2Image(currentPosition);
      //DebugText = string.Format("grid:({0:0.0},{1:0.0})  image:({2:0.0},{3:0.0})  CtrPix:({4:0.0},{5:0.0})  viewportSize:({6:0.0},{7:0.0})",
      //  currentPosition.X, currentPosition.Y,
      //  grid2image.X, grid2image.Y,
      //  CurrentDisplayCenterPosition.X, CurrentDisplayCenterPosition.Y,
      //  scrollViewer.ViewportWidth, scrollViewer.ViewportHeight);
      Point gridPosition = Image2Grid(imagePosition);
      DebugText = string.Format("grid:({0},{1})  image:({2},{3})  g2i2g;{8:0.0},{9:0.0})  CtrPix:({4:0.0},{5:0.0})  viewport:({6:0.0},{7:0.0})\n SelectRect {10},{11},{12},{13} inSelectRect {14}",
        currentPosition.X, currentPosition.Y,
        imagePosition.X, imagePosition.Y,
        CurrentDisplayCenterPosition.X, CurrentDisplayCenterPosition.Y,
        scrollViewer.ViewportWidth, scrollViewer.ViewportHeight,
        gridPosition.X, gridPosition.Y, SelectRect.X, SelectRect.Y, SelectRect.Width, SelectRect.Height, inSelectRect);
#endif
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);
      if (displayGrid.IsMouseCaptured)
      {
        displayGrid.ReleaseMouseCapture();
        //Point currentPosition = e.MouseDevice.GetPosition(this);
        Point currentPosition = e.GetPosition(this);
        bool noMotion = currentPosition.Equals(mouseDownPosition);
        bool altDown = Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
        if (!temporaryPanMode)
        {
          if (DragMode == DragModes.Select)
          {
            if (noMotion)
              SelectRect = Int32Rect.Empty;
            else
            {
              Point leftTop = FixImageScale(selectionCanvas.TranslatePoint(new Point(Canvas.GetLeft(selectRectangle), Canvas.GetTop(selectRectangle)), displayImage));
              int left = Math.Max(0, (int)leftTop.X);
              int top = Math.Max(0, (int)leftTop.Y);
              int width = Math.Min(displayBMI.PixelWidth - left, (int)Math.Round(selectRectangle.Width / CurrentScale));
              int height = Math.Min(displayBMI.PixelHeight - top, (int)Math.Round(selectRectangle.Height / CurrentScale));
              SelectRect =  new Int32Rect(left, top, width, height);
            }
          }
          else if (DragMode == DragModes.Zoom)
          {
            if (noMotion)
            {
              if (altDown)
                Zoom(NextZoomOutScale(), FixImageScale(e.GetPosition(displayImage)));
              else
                Zoom(NextZoomInScale(), FixImageScale(e.GetPosition(displayImage)));
            }
            else if (!altDown)
            {
              double scaleX = (zoomRectangle.Width > 0) ? scrollViewer.ViewportWidth / zoomRectangle.Width * CurrentScale : CurrentScale;
              double scaleY = (zoomRectangle.Height > 0) ? scrollViewer.ViewportHeight / zoomRectangle.Height * CurrentScale : CurrentScale;
              double centerX = Canvas.GetLeft(zoomRectangle) + zoomRectangle.Width / 2;
              double centerY = Canvas.GetTop(zoomRectangle) + zoomRectangle.Height / 2;
              double scale = Math.Min(scaleX, scaleY);
              Point imagePos = FixImageScale(selectionCanvas.TranslatePoint(new Point(centerX, centerY), displayImage));
              Zoom(scale, imagePos);
            }
          }
        }
        temporaryPanMode = false;
        zoomDragging = false;
        BestCursor();
      }
    }

    //-------------------------------------------------------------------------------------------------------
    protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
    {
      base.OnPreviewMouseWheel(e);
      if (Mouse.LeftButton == MouseButtonState.Released)
      {
        Zoom((e.Delta > 0) ? NextZoomInScale() : NextZoomOutScale(), CurrentDisplayCenterPosition);;
        e.Handled = true;
      }
    }

    //-------------------------------------------------------------------------------------------------------
    public Point GetCurrentImagePosition()
    {
      return Grid2Image(Mouse.GetPosition(this));
    }

    //-------------------------------------------------------------------------------------------------------
    protected Rect GenerateRect(Point start, Point end)
    {
      double left = Math.Min(start.X, end.X);
      double top = Math.Min(start.Y, end.Y);
      double right = Math.Max(start.X, end.X);
      double bottom = Math.Max(start.Y, end.Y);
      return new Rect(left, top, right - left, bottom - top);
    }

    //-------------------------------------------------------------------------------------------------------
    public Int32Rect ClipRectToImage(Int32Rect rect)
    {
      if (displayBMI != null)
      {
        int X = Math.Max(0, Math.Min(rect.X, displayBMI.PixelWidth - 1));
        int Y = Math.Max(0, Math.Min(rect.Y, displayBMI.PixelHeight - 1));
        int Width = Math.Min(rect.Width - (X - rect.X), displayBMI.PixelWidth - X);
        int Height = Math.Min(rect.Height - (Y - rect.Y), displayBMI.PixelHeight - Y);
        return new Int32Rect(X, Y, Width, Height);
      }
      return rect;
    }

    //-------------------------------------------------------------------------------------------------------
    public Int32Rect ClipRectToImage(Rect rect)
    {
      if (displayBMI != null)
      {
        int X = Math.Max(0, Math.Min((int)Math.Round(rect.X), displayBMI.PixelWidth - 1));
        int Y = Math.Max(0, Math.Min((int)Math.Round(rect.Y), displayBMI.PixelHeight - 1));
        int Width = Math.Min((int)Math.Round(rect.Width - (X - rect.X)), displayBMI.PixelWidth - X);
        int Height = Math.Min((int)Math.Round(rect.Height - (Y - rect.Y)), displayBMI.PixelHeight - Y);
        return new Int32Rect(X, Y, Width, Height);
      }
      return new Int32Rect((int)Math.Round(rect.X), (int)Math.Round(rect.Y), (int)Math.Round(rect.Width), (int)Math.Round(rect.Height));
    }

    //-------------------------------------------------------------------------------------------------------
    public BitmapSource CopySelection()
    {
      if (displayBMI == null)
        return null;

#if true
      // crop SelectRect to image boundaries
      var rect = SelectRect;
      if (rect.X < 0)
      {
        rect.Width += rect.X; // add negative to decrease width
        rect.X = 0;
      }
      if (rect.Y < 0)
      {
        rect.Height += rect.Y; // add negative to decrease height
        rect.Y = 0;
      }
      if (rect.X + rect.Width > displayBMI.PixelWidth)
        rect.Width = displayBMI.PixelWidth - rect.X;
      if (rect.Y + rect.Height > displayBMI.PixelHeight)
        rect.Height = displayBMI.PixelHeight - rect.Y;

      return ((rect.Width <= 0) || ((rect.Height <= 0))) ? displayBMI : new CroppedBitmap(displayBMI, SelectRect);
#elif true
      double originalScale = CurrentScale;
      Point originalLocation = CurrentDisplayCenterPosition;
      Visibility selectionVisibility = selectionCanvas.Visibility;
      selectionCanvas.Visibility = Visibility.Hidden;
      ScaleToFit();
      this.UpdateLayout();
      //RenderTargetBitmap rtb = new RenderTargetBitmap((int)Math.Ceiling(displayBMI.PixelWidth * displayOne2oneScale.X),
      //                                                (int)Math.Ceiling(displayBMI.PixelHeight * displayOne2oneScale.Y),
      //                                                windowsScreenDPI.X, windowsScreenDPI.Y, PixelFormats.Default);
      int width = (int)Math.Round(scrollViewer.ActualWidth / CurrentScale);
      int height = (int)Math.Round(scrollViewer.ActualHeight / CurrentScale);
      RenderTargetBitmap rtb = new RenderTargetBitmap(width, height, windowsScreenDPI.X, windowsScreenDPI.Y, PixelFormats.Default);
      rtb.Render(scrollViewer);

      //return (SelectRect.IsEmpty) ? rtb : new CroppedBitmap(rtb, SelectRect);
      Zoom(originalScale, originalLocation);
      selectionCanvas.Visibility = selectionVisibility;
      return rtb;
#else
      object clonedDisplayGrid;
      using (var stream = new MemoryStream())
      {
        XamlWriter.Save(displayGrid, stream);
        stream.Seek(0, SeekOrigin.Begin);
        clonedDisplayGrid = XamlReader.Load(stream);
      }
      RenderTargetBitmap rtb = new RenderTargetBitmap(displayBMI.PixelWidth, displayBMI.PixelHeight, 96, 96, PixelFormats.Default);
      rtb.Render(clonedDisplayGrid as UIElement);
      return rtb;
#endif
    }

    //-------------------------------------------------------------------------------------------------------
    public Style SelectionStyle
    {
      get { return selectRectangle.Style; }
      set { selectRectangle.Style = value; }
    }

    //-------------------------------------------------------------------------------------------------------
    public Style ZoomStyle
    {
      get { return zoomRectangle.Style; }
      set { zoomRectangle.Style = value; }
    }

    //-------------------------------------------------------------------------------------------------------
    protected void BestCursor()
    {
      if (Keyboard.IsKeyDown(Key.Space))
        Cursor = cursorHand;
      else
        switch (DragMode)
        {
          default:
          case DragModes.None: Cursor = Cursors.Arrow; break;
          case DragModes.Pan: Cursor = cursorHand; break;
          case DragModes.Select: Cursor = cursorSelectCross; break;
          case DragModes.Zoom: Cursor = (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt)) ? cursorZoomOut : cursorZoomIn; break;
        }
    }
  }

}
