﻿using System;
using System.Windows;
using System.Windows.Data;

namespace WpfUserControls
{
  //=========================================================================================================
  /// <summary>
  /// Linearly scales double arguments. This was created to work with the ViewBox which has trouble with
  /// small dimensions (it appears to use integer math somewhere).
  /// </summary>
  public class ScalingConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      if (value == null)
        return null;

      double val = (value is string) ? double.Parse(value as string) : (double)value;
      double scale = (parameter is string) ? double.Parse(parameter as string) : (double)parameter;
      double scaled = val * scale;
      return scaled;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double val = (value is string) ? double.Parse(value as string) : (double)value;
      double scale = (parameter is string) ? double.Parse(parameter as string) : (double)parameter;
      return val / scale;
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Adds to an integer value (intended to change from zero base index to one base index)
  /// </summary>
  public class OffsetConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      int offset = 1;
      if (parameter is string)
        int.TryParse(parameter as string, out offset);
      else if (parameter is int)
        offset = (int)parameter;
      return System.Convert.ToInt32(value) + offset;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      int offset = 1;
      if (parameter is string)
        int.TryParse(parameter as string, out offset);
      else if (parameter is int)
        offset = (int)parameter;
      //return Convert.ToInt32(value) - offset;
      int rval = (int)(double)value - offset;
      return rval;
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Adds a random offset to a double value (for debug use)
  /// </summary>
  public class RandomOffsetConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double range = 1;
      if (parameter is string)
        double.TryParse(parameter as string, out range);
      else 
        range = System.Convert.ToDouble(parameter);
      Random rand = new Random();
      return System.Convert.ToDouble(value) + rand.NextDouble() * range;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Used to invert boolean value (There must be a better way to do this.)
  /// </summary>
  public class BoolInvertConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return (value is bool) ? !(bool)value : false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return (value is bool) ? !(bool)value : true;
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Converts boolean dependency property to a visibility value (Visible or Collapsed)
  /// </summary>
  public class BoolToOpacityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double falseOpacity = 0.0;
      double.TryParse(parameter as string, out falseOpacity);
      return (bool)value ? 1.0 : falseOpacity;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Converts boolean dependency property to 0 or 180 degrees for rotating a control
  /// </summary>
  public class BoolTo180Converter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return (bool)value ? 180 : 0;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Converts boolean dependency property to a visibility value (Visible or Collapsed)
  /// </summary>
  public class BoolToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool invert = (parameter is string) && ((parameter as string).ToLower() == "invert");
      return (bool)value == invert ? Visibility.Collapsed : Visibility.Visible;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool invert = (parameter is string) && ((parameter as string).ToLower() == "invert");
      return ((Visibility)value == Visibility.Visible) != invert;
    }
  }

  //=========================================================================================================
  /// <summary>
  /// If the value is null or white space we return Visible otherwise Collapsed
  /// if the parameter is the text string "invert" the return values are reversed
  /// </summary>
  public class IsNullToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool invert = (parameter is string) && ((parameter as string).ToLower() == "invert");
      bool isNull = (value == null) || ((value is string) && (string.IsNullOrWhiteSpace(value as string)));
      return (isNull != invert) ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// If the value is null or white space we return true otherwise false
  /// if the parameter is the text string "invert" the return values are reversed
  /// </summary>
  public class IsNullToBoolConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      bool invert = (parameter is string) && ((parameter as string).ToLower() == "invert");
      bool isNull = (value == null) || ((value is string) && (string.IsNullOrWhiteSpace(value as string)));
      return isNull != invert;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is greater than the parameter.
  /// </summary>
  public class IsGreaterThanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) > compareValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is less than the parameter.
  /// </summary>
  public class IsLessThanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) < compareValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is less than the parameter.
  /// </summary>
  public class IsEqualConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) == compareValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is greater than the parameter.
  /// </summary>
  public class VisibilityGreaterThanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) > compareValue ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is less than the parameter.
  /// </summary>
  public class VisibilityLessThanConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) < compareValue ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// The value and parameters are parsed to doubles. Returns true if the value is less than the parameter.
  /// </summary>
  public class VisibilityEqualConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      double compareValue = 0;
      if (parameter is string)
        compareValue = double.Parse(parameter as string);
      return System.Convert.ToDouble(value) == compareValue ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Compares the type of the value to the type of the parameter. Returns true if they are the same.
  /// </summary>
  public class IsTypeToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return ((value != null) && (value.GetType() == (System.Type)parameter)) ? Visibility.Visible : Visibility.Collapsed;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }

  //=========================================================================================================
  /// <summary>
  /// Adds a random offset to a double value (for debug use)
  /// </summary>
  public class NullToGrayConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return value == null ? System.Windows.Media.Brushes.Gray : System.Windows.Media.Brushes.Black;
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}

