﻿using System;
using System.Windows.Input;

namespace WpfUserControls
{
  //-----------------------------------------------------------------------------------------------
  // I am sure that there is a better way!
  // I accept my ignorance and forge on undaunted!

  //-----------------------------------------------------------------------------------------------
  public class DelegateCommand : ICommand
  {
    private readonly Action _execute;
    private readonly Func<bool> _canExecute;

    public event EventHandler CanExecuteChanged;

    public DelegateCommand(Action execute) : this(execute, null)
    {
    }

    public DelegateCommand(Action execute,
                           Func<bool> canExecute)
    {
      _execute = execute;
      _canExecute = canExecute;
    }

    public bool CanExecute(object parameter)
    {
      if (_canExecute == null)
      {
        return true;
      }

      return _canExecute();
    }

    public void Execute(object parameter)
    {
      _execute();
    }

    public void RaiseCanExecuteChanged()
    {
      if (CanExecuteChanged != null)
      {
        CanExecuteChanged(this, EventArgs.Empty);
      }
    }
  }

  //-----------------------------------------------------------------------------------------------
  public class PDelegateCommand : ICommand
  {
    private readonly Action<object> _execute;
    private readonly Predicate<object> _canExecute;

    public event EventHandler CanExecuteChanged;

    public PDelegateCommand(Action<object> execute)
                   : this(execute, null)
    {
    }

    public PDelegateCommand(Action<object> execute,
                           Predicate<object> canExecute)
    {
      _execute = execute;
      _canExecute = canExecute;
    }

    public bool CanExecute(object parameter)
    {
      return (_canExecute == null) ? true : _canExecute(parameter);
    }

    public void Execute(object parameter)
    {
      _execute(parameter);
    }

    public void RaiseCanExecuteChanged()
    {
      if (CanExecuteChanged != null)
      {
        CanExecuteChanged(this, EventArgs.Empty);
      }
    }
  }

}
