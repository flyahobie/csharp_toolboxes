﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using WpfUserControls;

namespace ImagePSGuiExample
{
  //=========================================================================================================
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    //-------------------------------------------------------------------------------------------------------
    protected ExampleViewModel viewModel = new ExampleViewModel();

    //-------------------------------------------------------------------------------------------------------
    public MainWindow()
    {
      InitializeComponent();
      viewModel.View = this;
      DataContext = viewModel;
    }

    //-------------------------------------------------------------------------------------------------------
    private void Window_DragEnter(object sender, DragEventArgs e)
    {
      if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
      {
        // limit it to one jpeg file
        string[] dropfiles = e.Data.GetData(DataFormats.FileDrop) as string[];
        if ((dropfiles != null) && (dropfiles.Length == 1))
        {
          string ext = System.IO.Path.GetExtension(dropfiles[0]).ToLower();
          if ((ext == ".jpg") || (ext == ".jpeg"))
            // allow them to continue
            // (without this, the cursor stays a "NO" symbol
            e.Effects = DragDropEffects.All;
        }
      }
    }

    //-------------------------------------------------------------------------------------------------------
    private void Window_Drop(object sender, DragEventArgs e)
    {
      string[] dropfiles = e.Data.GetData(DataFormats.FileDrop) as string[];
      if ((dropfiles != null) && (dropfiles.Length > 0))
        viewModel.OpenImage(dropfiles[0]);
    }

    //-------------------------------------------------------------------------------------------------------
    private void OpenImage_Click(object sender, RoutedEventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.Filter = "JPEG(*.jpg)|*.jpg;*.jpeg";
      Nullable<bool> result = openFileDialog.ShowDialog();
      if (result == true)
        viewModel.OpenImage(openFileDialog.FileName);
    }

    //-------------------------------------------------------------------------------------------------------
    private void arrowButton_Click(object sender, RoutedEventArgs e)
    {
      viewModel.arrowButton_Click();
    }

    //-------------------------------------------------------------------------------------------------------
    private void selectButton_Click(object sender, RoutedEventArgs e)
    {
      viewModel.selectButton_Click();
    }

    //-------------------------------------------------------------------------------------------------------
    private void zoomButton_Click(object sender, RoutedEventArgs e)
    {
      viewModel.zoomButton_Click();
    }

    //-------------------------------------------------------------------------------------------------------
    private void zoomButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      viewModel.zoomButton_MouseDoubleClick();
    }

    //-------------------------------------------------------------------------------------------------------
    private void panButton_Click(object sender, RoutedEventArgs e)
    {
      viewModel.panButton_Click();
    }

    //-------------------------------------------------------------------------------------------------------
    private void panButton_MouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
      viewModel.panButton_MouseDoubleClick();
    }

    //-------------------------------------------------------------------------------------------------------
    private void SaveSelection_Click(object sender, RoutedEventArgs e)
    {
      SaveFileDialog saveFileDialog = new SaveFileDialog();
      saveFileDialog.Filter = "JPEG(*.jpg)|*.jpg;*.jpeg";
      Nullable<bool> result = saveFileDialog.ShowDialog();
      if (result == true)
        viewModel.SaveSelection(saveFileDialog.FileName);
    }

    //-------------------------------------------------------------------------------------------------------
    private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
    {
      imagePS.DataContext = this.DataContext;
    }

    //-------------------------------------------------------------------------------------------------------
    private void ClearDots_Click(object sender, RoutedEventArgs e)
    {
      viewModel.ClearUserItems();
    }

  }
}
