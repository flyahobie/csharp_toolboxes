﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfUserControls;

namespace ImagePSGuiExample
{
  //=========================================================================================================
  public class ExampleViewModel : INotifyPropertyChanged
  {
    //-------------------------------------------------------------------------------------------------------
    protected double radius = 4;
    protected ObservableCollection<UIElement> userItems = new ObservableCollection<UIElement>();
    protected UIElement selectedItem = null;

    //-------------------------------------------------------------------------------------------------------
    protected MainWindow _view = null;
    public MainWindow View 
    {
      get { return _view; }
      set { if (_view != value) { _view = value; InitView(); NotifyPropertyChanged("View"); } }
    }

    //-------------------------------------------------------------------------------------------------------
    protected string _filename = null;
    public string Filename
    {
      get { return _filename; }
      set { if (_filename != value) { _filename = value; NotifyPropertyChanged("Filename"); } }
    }

    //-------------------------------------------------------------------------------------------------------
    protected string _title = null;
    public string Title
    {
      get { return _title; }
      set { if (_title != value) { _title = value; NotifyPropertyChanged("Title"); } }
    }

    //-------------------------------------------------------------------------------------------------------
    protected string _debugText = null;
    public string DebugText
    {
      get { return _debugText; }
      set { if (_debugText != value) { _debugText = value; NotifyPropertyChanged("DebugText"); } }
    }

    //-------------------------------------------------------------------------------------------------------
    protected BitmapImage _imageSource = null;
    public BitmapImage ImageSource
    {
      get { return _imageSource; }
      set 
      { 
        if (_imageSource != value) 
        { 
          _imageSource = value;
          NotifyPropertyChanged("ImageSource"); 
        } 
      }
    }

    //-------------------------------------------------------------------------------------------------------
    public ExampleViewModel()
    {
      InitView();
    }

    //-------------------------------------------------------------------------------------------------------
    protected void InitView()
    {
      if (View != null)
      {
        View.imagePS.CurrentScaleChanged += new EventHandler(imagePS_CurrentScaleChanged);
        View.imagePS.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(imagePS_PreviewMouseDown);
        View.imagePS.PreviewMouseMove += new System.Windows.Input.MouseEventHandler(imagePS_PreviewMouseMove);
        View.imagePS.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(imagePS_PreviewMouseUp);
        View.imagePS.BitmapScalingMode = BitmapScalingMode.NearestNeighbor;
        View.imagePS.DragMode = ImagePS.DragModes.None;
        //View.imagePS.UserItems = new System.Collections.ObjectModel.ObservableCollection<System.Windows.UIElement>();
        View.imagePS.UserItems = userItems;
      }
    }

    //------------------------------------------------------------------
    void imagePS_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      // We don't want to pick up events from the scrollbars
      if (!View.imagePS.displayGrid.IsMouseOver)
        return;

      System.Windows.Point position = View.imagePS.GetCurrentImagePosition();
      DebugText = string.Format("({0:0.0},{1:0.0}) Down", position.X, position.Y);

      if (View.imagePS.DragMode == ImagePS.DragModes.None)
      {
        selectedItem = GetSelectedElement(position);
        if ((selectedItem == null) && (Keyboard.Modifiers == ModifierKeys.None) && !Keyboard.IsKeyDown(Key.Space))
        {
          selectedItem = new Ellipse() { Width = 2 * radius, Height = 2 * radius, Fill = Brushes.Red, StrokeThickness = 0, RenderTransform = new TranslateTransform(-radius, -radius) };
          userItems.Add(selectedItem);
        }
        else if ((selectedItem != null) && (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)))
        {
          userItems.Remove(selectedItem);
          selectedItem = null;
        }
        if (selectedItem != null)
        {
          Canvas.SetLeft(selectedItem, position.X);
          Canvas.SetTop(selectedItem, position.Y);
        }
      }
    }

    //------------------------------------------------------------------
    void imagePS_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    {
      System.Windows.Point position = View.imagePS.GetCurrentImagePosition();
      DebugText = string.Format("({0:0.0},{1:0.0}) Move", position.X, position.Y);
      if (selectedItem != null)
      {
        Canvas.SetLeft(selectedItem, position.X);
        Canvas.SetTop(selectedItem, position.Y);
      }
    }

    //------------------------------------------------------------------
    void imagePS_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      selectedItem = null;
    }

    //------------------------------------------------------------------
    protected UIElement GetSelectedElement(Point location)
    {
      UIElement element = null;
      for (int ii = 0; (element == null) && (ii < userItems.Count); ++ii)
      {
        double top = Canvas.GetTop(userItems[ii]);
        double left = Canvas.GetLeft(userItems[ii]);
        if ((top - radius <= location.Y) && (location.Y <= top + radius) && (left - radius <= location.X) && (location.X <= left + radius))
          element = userItems[ii];
      }
      return element;
    }

    //------------------------------------------------------------------
    void imagePS_CurrentScaleChanged(object sender, EventArgs e)
    {
      double scale = View.imagePS.CurrentScale;
      string format = (scale < 1) ? "{0} @ {1:0.00%}" : "{0} @ {1:0%}";
      Title = string.Format(format, System.IO.Path.GetFileName(Filename), scale);
    }

    //-------------------------------------------------------------------------------------------------------
    public void OpenImage(string filename)
    {
      Filename = filename;
      ImageSource = new BitmapImage(new Uri(filename));
    }

    //-------------------------------------------------------------------------------------------------------
    public void SaveSelection(string filename)
    {
      BitmapSource bmpSrc = View.imagePS.CopySelection();
      // Creates a JpegBitmapEncoder and adds the BitmapSource to the frames of the encoder
      JpegBitmapEncoder jpegBitmapEncoder = new JpegBitmapEncoder();
      jpegBitmapEncoder.QualityLevel = 97;
      jpegBitmapEncoder.Frames.Add(BitmapFrame.Create(bmpSrc));

      // Saves the image into a file using the encoder
      using (Stream stream = File.Create(filename))
        jpegBitmapEncoder.Save(stream);
    }

    //-------------------------------------------------------------------------------------------------------
    public void ClearUserItems()
    {
      userItems.Clear();
    }

    //-------------------------------------------------------------------------------------------------------
    public void arrowButton_Click()
    {
      View.imagePS.DragMode = ImagePS.DragModes.None;
    }

    //-------------------------------------------------------------------------------------------------------
    public void selectButton_Click()
    {
      View.imagePS.DragMode = ImagePS.DragModes.Select;
    }

    //-------------------------------------------------------------------------------------------------------
    public void zoomButton_Click()
    {
      View.imagePS.DragMode = ImagePS.DragModes.Zoom;
    }

    //-------------------------------------------------------------------------------------------------------
    public void zoomButton_MouseDoubleClick()
    {
      View.imagePS.ScaleToFit();
    }

    //-------------------------------------------------------------------------------------------------------
    public void panButton_Click()
    {
      View.imagePS.DragMode = ImagePS.DragModes.Pan;
    }

    //-------------------------------------------------------------------------------------------------------
    public void panButton_MouseDoubleClick()
    {
      View.imagePS.Zoom(1, View.imagePS.CurrentDisplayCenterPosition);
    }

    //-------------------------------------------------------------------------------------------------------
    /// <summary>
    /// INotofyPropertyChanged event
    /// </summary>
    public event PropertyChangedEventHandler PropertyChanged;

    //-------------------------------------------------------------------------------------------------------
    public void NotifyPropertyChanged(String propertyName)
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
    }  
  }
}
