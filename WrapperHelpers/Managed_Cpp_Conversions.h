// Author: Peter Stubler 

#pragma once

#pragma managed(push, off)
#include <string>
#include <vector>
#pragma managed(pop)


//============================================================================================
// Important Note
// This header includes inline and template code that will be incorporated into the refering
// file that includes it. Thus, it does not need to be resolved at link time and adopts the 
// namespace of the including file. This also means that the code will be repeated in different
// modules which is acceptable due to its small size. This may not be the case for other parts
// of this library.
//============================================================================================

/* -------------------------------------------------------------------------------------------
Passing By Reference

Declared In managed C++ as:
		static bool IsCandidateStereoPair(
			System::Drawing::Bitmap^ leftImage,
			System::Drawing::Bitmap^ rightImage,
			int timeThreshold,
			int% xOffset,
			int% yOffset)

values assigned in managed C++ like this:
			xOffset = 123;
			yOffset = 456;


Called In C# as:
		IsCandidateStereoPair(leftImage, rightImage, timeThreshold, ref xOffset, ref yOffset);
//--------------------------------------------------------------------------------------------
*/


//===========================================================================================================
// General C#/C++ 

// Stolen from http://www.cs.virginia.edu/~cs216/labs/helpdocs/system.string.html
//-----------------------------------------------------------------------------------------------------------
inline System::String ^Cpp2Managed(const std::string &stlstring)
{
  //the c_str() function gets a char array from the std::string,
  //but the PtrToStringAnsi function wants a int array, so it gets casted
  //return Marshal::PtrToStringAnsi((System::IntPtr) stlstring.c_str());
  return System::Runtime::InteropServices::Marshal::PtrToStringAnsi(static_cast<System::IntPtr>(const_cast<char*>(stlstring.c_str())));
}

//-----------------------------------------------------------------------------------------------------------
/// <summary>
/// A function for marshalling an unmanaged cpp vector of vectors to a managed CLI array of arrays
/// </summary>
/// <param name="std_vec">the std::vector</param>
/// <returns>a CLI array</returns>
inline cli::array<System::String^> ^Cpp2Managed(const std::vector<std::string> &std_vec)
{
  cli::array<System::String^> ^mgd_arr = gcnew cli::array<System::String^>((int)std_vec.size());
  for (unsigned ii = 0; ii < std_vec.size(); ++ii)
    mgd_arr[ii] = Cpp2Managed(std_vec[ii]);
  return mgd_arr;
}

// Stolen from http://www.cs.virginia.edu/~cs216/labs/helpdocs/system.string.html
// later modified
//-----------------------------------------------------------------------------------------------------------
// Converts a System::String to a std::string
// This code assumes that you have used the following namespace:
// using namespace System::Runtime::InteropServices;
inline std::string Managed2Cpp(System::String ^managed) 
{
  std::string stl;

  if (managed != nullptr)
  {
    //get a pointer to an array of ANSI chars
    char* chars = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(managed).ToPointer();

    //assign the array to an std::string
    stl = chars;

    //free the memory used by the array
    //since the array is not managed, it will not be claimed by the garbage collector
    System::Runtime::InteropServices::Marshal::FreeHGlobal(static_cast<System::IntPtr>(chars));
  }
  return stl;
}

//-----------------------------------------------------------------------------------------------------------

/// <summary>
/// A function for marshalling a managed CLI array of arrays to an unmanaged cpp vector of vectors
/// </summary>
/// <param name="mgd_arr">CLI array</param>
/// <returns>std::vector</returns>
inline std::vector<std::string> Managed2Cpp(cli::array<System::String ^> ^mgd_arr)
{
  std::vector<std::string> std_vec;
  std_vec.reserve(mgd_arr->Length);
  for (int ii = 0; ii < mgd_arr->Length; ++ii)
    std_vec.push_back(Managed2Cpp(mgd_arr[ii]));
  return std_vec;
}

//-----------------------------------------------------------------------------------------------------------
/// <summary>
/// A function for marshalling an unmanaged cpp vector to a managed CLI array
/// </summary>
/// <typeparam name="T">the type</typeparam>
/// <param name="std_vec">the std::vector</param>
/// <returns>a CLI array</returns>
template <typename T> 
cli::array<T> ^Cpp2Managed(const std::vector<T> &std_vec)
{
  cli::array<T> ^mgd_arr = gcnew cli::array<T>((int)std_vec.size());
  for (unsigned ii = 0; ii < std_vec.size(); ++ii)
    mgd_arr[ii] = std_vec[ii];
  return mgd_arr;
}

//-----------------------------------------------------------------------------------------------------------
/// <summary>
/// A function for marshalling a managed CLI array to an unmanaged cpp vector
/// </summary>
/// <typeparam name="T">the type</typeparam>
/// <param name="mgd_arr">managed CLI array</param>
/// <returns>std::vector</returns>
template <typename T> 
std::vector<T> Managed2Cpp(cli::array<T> ^mgd_arr)
{
  std::vector<T> std_vec;
  std_vec.reserve(mgd_arr->Length);
  for (int ii = 0; ii < mgd_arr->Length; ++ii)
    std_vec.push_back(mgd_arr[ii]);
  return std_vec;
}

#if 0
//-----------------------------------------------------------------------------------------------------------
template <typename MT, typename CT> 
cli::array<MT> ^ Cpp2Managed(const std::vector<CT> &std_vec)
{
  cli::array<MT> ^mgd_arr = gcnew cli::array<T>((int)std_vec.size());
  for (unsigned ii = 0; ii < std_vec.size(); ++ii)
    mgd_arr[ii] = Cpp2Managed(std_vec[ii]);
  return mgd_arr;
}

//-----------------------------------------------------------------------------------------------------------
template <typename CT, typename MT> 
std::vector<CT> Managed2Cpp(cli::array<MT> ^mgd_arr)
{
  std::vector<CT> std_vec;
  std_vec.reserve(mgd_arr->Length);
  for (int ii = 0; ii < mgd_arr->Length; ++ii)
    std_vec.push_back(Managed2Cpp(mgd_arr[ii]));
  return std_vec;
}
#endif