// Author: Peter Stubler

#pragma once

#ifdef _MANAGED
#pragma managed(push, off)
#include <ctime>
//#include "boost/date_time/posix_time/posix_time.hpp"
#pragma managed(pop)
#endif

#using <WindowsBase.dll>
#using <PresentationCore.dll>
using namespace System;
using namespace System::Windows;
using namespace System::Windows::Media::Imaging;
using namespace System::Runtime::InteropServices;


namespace WrapperHelpers
{
  //-----------------------------------------------------------------------------------------------------------
  // time conversions

  //-----------------------------------------------------------------------------------------------------------
  /// <summary>
  /// converts a cpp time to a CLI DateTime
  /// </summary>
  /// <param name="theTime"></param>
  /// <param name="local"></param>
  /// <returns></returns>
  inline System::DateTime Time2DateTime(time_t theTime, bool local = true)
  {
    System::DateTime dateTime;
    long timezone = 0;
    if (!local)
      _get_timezone(&timezone);
    time_t gmtime = theTime + timezone;
    struct tm tms;
    if ((theTime != 0) && !gmtime_s(&tms, &gmtime))
      dateTime = System::DateTime(tms.tm_year + 1900, tms.tm_mon + 1, tms.tm_mday, tms.tm_hour, tms.tm_min, tms.tm_sec);
    return dateTime;
  }

  //-----------------------------------------------------------------------------------------------------------
  /// <summary>
  /// converts a CLI DateTime to a cpp time
  /// </summary>
  /// <param name="dateTime"></param>
  /// <param name="local"></param>
  /// <returns></returns>
  inline time_t DateTime2Time(System::DateTime dateTime, bool local = true)
  {
    struct tm tms;
    tms.tm_isdst = 0;
    tms.tm_year = dateTime.Year - 1900;
    tms.tm_mon = dateTime.Month - 1;
    tms.tm_mday = dateTime.Day;
    tms.tm_hour = dateTime.Hour;
    tms.tm_min = dateTime.Minute;
    tms.tm_sec = dateTime.Second;
    long timezone = 0;
    if (!local)
      _get_timezone(&timezone);
    return mktime(&tms) - timezone;
  }

#if 0
  //-----------------------------------------------------------------------------------------------------------
  inline System::Nullable<System::DateTime> PTime2DateTime(const boost::posix_time::ptime& pt)
  {
    if (pt.is_not_a_date_time())
      return System::Nullable<System::DateTime>();

    boost::gregorian::date theDate = pt.date();
    boost::posix_time::time_duration theTimeOfDay = pt.time_of_day();
    return System::DateTime(theDate.year(), theDate.month(), theDate.day(),
      theTimeOfDay.hours(), theTimeOfDay.minutes(), theTimeOfDay.seconds(), (int)(theTimeOfDay.fractional_seconds() * 1000));
  }


  //-----------------------------------------------------------------------------------------------------------
  inline boost::posix_time::ptime DateTime2PTime(System::Nullable<System::DateTime> dt)
  {
    if (!dt.HasValue)
      return boost::posix_time::not_a_date_time;

    System::DateTime dateTime = (System::DateTime)dt;
    boost::gregorian::date theDate(dateTime.Year, dateTime.Month, dateTime.Day);
    boost::posix_time::time_duration theTimeOfDay(dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond);
    return boost::posix_time::ptime(theDate, theTimeOfDay);
  }
#endif
}
