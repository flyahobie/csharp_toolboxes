// Author: Peter Stubler

===========================================================================================
Important Note

This library is intended to be included as a helper for other wrapper libraries.
The purpose is to facilitate the schleping of data between the managed and unmanaged
memory spaces. 

This library includes headers that provide inline and template code that will be 
incorporated into the refering file that includes it. Thus, these functions do not 
need to be resolved at link time since they adopt the namespace of the including file. 
This also means that the code will be repeated in different modules which I believe
is acceptable due to its small size. 

This library also includes headers and source files that define managed classes that
have their own namespace and code that is compiled and resolved at the link time of this
library. 

===========================================================================================
Important(er) Note
You will notice pragma statements in the wrapper headers and source code.
These statements are VERY important and control how the compiler treats
functions with respect to managed vs non-managed. The rules of thumb are this:

	1) turn on /CLR (Common Language Runtime) for the wrapper project
	2) place include statements to non-managed (traditional) headers
	   between #pragma managed statements as show below.

On a personal style note, you will see that I consistently use "#pragma once"
in wrapper headers and more traditional #ifndef, #define, #endif statements
for traditional C++. If I had a reason other than style, I have forgotten.

EXAMPLE:
//-----------------------------------------------------------------------------------------
// Author: Peter Stubler 

#pragma once

#pragma managed(push, off)
#include <vector>
// any non-managed (non-CLR) headers
#pragma managed(pop)

#include "Managed_Std_Conversions.h"
// any managed (CLR) headers